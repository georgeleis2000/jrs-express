﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDashboard
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDashboard))
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.adminBox1 = New System.Windows.Forms.GroupBox()
        Me.btnTransact = New System.Windows.Forms.Button()
        Me.btnOpenship = New System.Windows.Forms.Button()
        Me.btnNewship = New System.Windows.Forms.Button()
        Me.adminBox2 = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnManifest = New System.Windows.Forms.Button()
        Me.btnConsign = New System.Windows.Forms.Button()
        Me.adminBox3 = New System.Windows.Forms.GroupBox()
        Me.btnDeliver = New System.Windows.Forms.Button()
        Me.btnEntry = New System.Windows.Forms.Button()
        Me.btnLogout = New System.Windows.Forms.Button()
        Me.userBox1 = New System.Windows.Forms.GroupBox()
        Me.btnConsignment = New System.Windows.Forms.Button()
        Me.userBox2 = New System.Windows.Forms.GroupBox()
        Me.btnManifest2 = New System.Windows.Forms.Button()
        Me.btnReports = New System.Windows.Forms.Button()
        Me.btnDestCon = New System.Windows.Forms.Button()
        Me.btnDRSReports = New System.Windows.Forms.Button()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.adminBox1.SuspendLayout()
        Me.adminBox2.SuspendLayout()
        Me.adminBox3.SuspendLayout()
        Me.userBox1.SuspendLayout()
        Me.userBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(-1, 1)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(626, 166)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'adminBox1
        '
        Me.adminBox1.Controls.Add(Me.btnTransact)
        Me.adminBox1.Controls.Add(Me.btnOpenship)
        Me.adminBox1.Controls.Add(Me.btnNewship)
        Me.adminBox1.Enabled = False
        Me.adminBox1.Location = New System.Drawing.Point(2, 182)
        Me.adminBox1.Name = "adminBox1"
        Me.adminBox1.Size = New System.Drawing.Size(210, 159)
        Me.adminBox1.TabIndex = 10
        Me.adminBox1.TabStop = False
        Me.adminBox1.Text = "Getting Started"
        Me.adminBox1.Visible = False
        '
        'btnTransact
        '
        Me.btnTransact.BackColor = System.Drawing.SystemColors.Window
        Me.btnTransact.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnTransact.Image = CType(resources.GetObject("btnTransact.Image"), System.Drawing.Image)
        Me.btnTransact.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnTransact.Location = New System.Drawing.Point(6, 29)
        Me.btnTransact.Name = "btnTransact"
        Me.btnTransact.Size = New System.Drawing.Size(198, 23)
        Me.btnTransact.TabIndex = 10
        Me.btnTransact.Text = "View All Transactions"
        Me.btnTransact.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnTransact.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnTransact.UseVisualStyleBackColor = False
        '
        'btnOpenship
        '
        Me.btnOpenship.BackColor = System.Drawing.SystemColors.Window
        Me.btnOpenship.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnOpenship.Image = CType(resources.GetObject("btnOpenship.Image"), System.Drawing.Image)
        Me.btnOpenship.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnOpenship.Location = New System.Drawing.Point(6, 87)
        Me.btnOpenship.Name = "btnOpenship"
        Me.btnOpenship.Size = New System.Drawing.Size(198, 23)
        Me.btnOpenship.TabIndex = 12
        Me.btnOpenship.Text = "Search Shipment"
        Me.btnOpenship.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnOpenship.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnOpenship.UseVisualStyleBackColor = False
        '
        'btnNewship
        '
        Me.btnNewship.BackColor = System.Drawing.SystemColors.Window
        Me.btnNewship.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnNewship.Image = CType(resources.GetObject("btnNewship.Image"), System.Drawing.Image)
        Me.btnNewship.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNewship.Location = New System.Drawing.Point(6, 58)
        Me.btnNewship.Name = "btnNewship"
        Me.btnNewship.Size = New System.Drawing.Size(198, 23)
        Me.btnNewship.TabIndex = 11
        Me.btnNewship.Text = "Create New Shipment"
        Me.btnNewship.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNewship.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnNewship.UseVisualStyleBackColor = False
        '
        'adminBox2
        '
        Me.adminBox2.Controls.Add(Me.Button1)
        Me.adminBox2.Controls.Add(Me.btnManifest)
        Me.adminBox2.Controls.Add(Me.btnConsign)
        Me.adminBox2.Enabled = False
        Me.adminBox2.Location = New System.Drawing.Point(218, 182)
        Me.adminBox2.Name = "adminBox2"
        Me.adminBox2.Size = New System.Drawing.Size(200, 159)
        Me.adminBox2.TabIndex = 11
        Me.adminBox2.TabStop = False
        Me.adminBox2.Text = "Source"
        Me.adminBox2.Visible = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.SystemColors.Window
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.Location = New System.Drawing.Point(6, 87)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(188, 23)
        Me.Button1.TabIndex = 13
        Me.Button1.Text = "Search Consignment"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.Button1.UseVisualStyleBackColor = False
        '
        'btnManifest
        '
        Me.btnManifest.BackColor = System.Drawing.SystemColors.Window
        Me.btnManifest.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnManifest.Image = CType(resources.GetObject("btnManifest.Image"), System.Drawing.Image)
        Me.btnManifest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnManifest.Location = New System.Drawing.Point(6, 58)
        Me.btnManifest.Name = "btnManifest"
        Me.btnManifest.Size = New System.Drawing.Size(188, 23)
        Me.btnManifest.TabIndex = 1
        Me.btnManifest.Text = "View Manifest Reports"
        Me.btnManifest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnManifest.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnManifest.UseVisualStyleBackColor = False
        '
        'btnConsign
        '
        Me.btnConsign.BackColor = System.Drawing.SystemColors.Window
        Me.btnConsign.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnConsign.Image = CType(resources.GetObject("btnConsign.Image"), System.Drawing.Image)
        Me.btnConsign.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnConsign.Location = New System.Drawing.Point(6, 29)
        Me.btnConsign.Name = "btnConsign"
        Me.btnConsign.Size = New System.Drawing.Size(188, 23)
        Me.btnConsign.TabIndex = 0
        Me.btnConsign.Text = "View Consignment Notes"
        Me.btnConsign.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnConsign.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnConsign.UseVisualStyleBackColor = False
        '
        'adminBox3
        '
        Me.adminBox3.Controls.Add(Me.btnDeliver)
        Me.adminBox3.Controls.Add(Me.btnEntry)
        Me.adminBox3.Enabled = False
        Me.adminBox3.Location = New System.Drawing.Point(425, 182)
        Me.adminBox3.Name = "adminBox3"
        Me.adminBox3.Size = New System.Drawing.Size(200, 159)
        Me.adminBox3.TabIndex = 12
        Me.adminBox3.TabStop = False
        Me.adminBox3.Text = "Destination"
        Me.adminBox3.Visible = False
        '
        'btnDeliver
        '
        Me.btnDeliver.BackColor = System.Drawing.SystemColors.Window
        Me.btnDeliver.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnDeliver.Image = CType(resources.GetObject("btnDeliver.Image"), System.Drawing.Image)
        Me.btnDeliver.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeliver.Location = New System.Drawing.Point(6, 58)
        Me.btnDeliver.Name = "btnDeliver"
        Me.btnDeliver.Size = New System.Drawing.Size(188, 23)
        Me.btnDeliver.TabIndex = 2
        Me.btnDeliver.Text = "DRS Reports"
        Me.btnDeliver.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeliver.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnDeliver.UseVisualStyleBackColor = False
        '
        'btnEntry
        '
        Me.btnEntry.BackColor = System.Drawing.SystemColors.Window
        Me.btnEntry.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnEntry.Image = CType(resources.GetObject("btnEntry.Image"), System.Drawing.Image)
        Me.btnEntry.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEntry.Location = New System.Drawing.Point(6, 29)
        Me.btnEntry.Name = "btnEntry"
        Me.btnEntry.Size = New System.Drawing.Size(188, 23)
        Me.btnEntry.TabIndex = 1
        Me.btnEntry.Text = "Entry Form"
        Me.btnEntry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEntry.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnEntry.UseVisualStyleBackColor = False
        '
        'btnLogout
        '
        Me.btnLogout.BackColor = System.Drawing.SystemColors.Window
        Me.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnLogout.Image = CType(resources.GetObject("btnLogout.Image"), System.Drawing.Image)
        Me.btnLogout.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLogout.Location = New System.Drawing.Point(537, 350)
        Me.btnLogout.Name = "btnLogout"
        Me.btnLogout.Size = New System.Drawing.Size(88, 23)
        Me.btnLogout.TabIndex = 14
        Me.btnLogout.Text = "Log Out"
        Me.btnLogout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnLogout.UseVisualStyleBackColor = False
        '
        'userBox1
        '
        Me.userBox1.Controls.Add(Me.btnReports)
        Me.userBox1.Controls.Add(Me.btnManifest2)
        Me.userBox1.Controls.Add(Me.btnConsignment)
        Me.userBox1.Enabled = False
        Me.userBox1.Location = New System.Drawing.Point(2, 379)
        Me.userBox1.Name = "userBox1"
        Me.userBox1.Size = New System.Drawing.Size(305, 159)
        Me.userBox1.TabIndex = 15
        Me.userBox1.TabStop = False
        Me.userBox1.Text = "Source"
        Me.userBox1.Visible = False
        '
        'btnConsignment
        '
        Me.btnConsignment.BackColor = System.Drawing.SystemColors.Window
        Me.btnConsignment.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnConsignment.Image = CType(resources.GetObject("btnConsignment.Image"), System.Drawing.Image)
        Me.btnConsignment.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnConsignment.Location = New System.Drawing.Point(9, 19)
        Me.btnConsignment.Name = "btnConsignment"
        Me.btnConsignment.Size = New System.Drawing.Size(289, 23)
        Me.btnConsignment.TabIndex = 17
        Me.btnConsignment.Text = "Consignment Notes"
        Me.btnConsignment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnConsignment.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnConsignment.UseVisualStyleBackColor = False
        '
        'userBox2
        '
        Me.userBox2.Controls.Add(Me.btnDRSReports)
        Me.userBox2.Controls.Add(Me.btnDestCon)
        Me.userBox2.Enabled = False
        Me.userBox2.Location = New System.Drawing.Point(314, 379)
        Me.userBox2.Name = "userBox2"
        Me.userBox2.Size = New System.Drawing.Size(311, 159)
        Me.userBox2.TabIndex = 16
        Me.userBox2.TabStop = False
        Me.userBox2.Text = "Destination"
        Me.userBox2.Visible = False
        '
        'btnManifest2
        '
        Me.btnManifest2.BackColor = System.Drawing.SystemColors.Window
        Me.btnManifest2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnManifest2.Image = CType(resources.GetObject("btnManifest2.Image"), System.Drawing.Image)
        Me.btnManifest2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnManifest2.Location = New System.Drawing.Point(9, 48)
        Me.btnManifest2.Name = "btnManifest2"
        Me.btnManifest2.Size = New System.Drawing.Size(289, 23)
        Me.btnManifest2.TabIndex = 18
        Me.btnManifest2.Text = "Manifest"
        Me.btnManifest2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnManifest2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnManifest2.UseVisualStyleBackColor = False
        '
        'btnReports
        '
        Me.btnReports.BackColor = System.Drawing.SystemColors.Window
        Me.btnReports.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnReports.Image = CType(resources.GetObject("btnReports.Image"), System.Drawing.Image)
        Me.btnReports.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnReports.Location = New System.Drawing.Point(9, 77)
        Me.btnReports.Name = "btnReports"
        Me.btnReports.Size = New System.Drawing.Size(289, 23)
        Me.btnReports.TabIndex = 19
        Me.btnReports.Text = "Reports"
        Me.btnReports.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnReports.UseVisualStyleBackColor = False
        '
        'btnDestCon
        '
        Me.btnDestCon.BackColor = System.Drawing.SystemColors.Window
        Me.btnDestCon.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnDestCon.Image = CType(resources.GetObject("btnDestCon.Image"), System.Drawing.Image)
        Me.btnDestCon.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDestCon.Location = New System.Drawing.Point(13, 19)
        Me.btnDestCon.Name = "btnDestCon"
        Me.btnDestCon.Size = New System.Drawing.Size(289, 23)
        Me.btnDestCon.TabIndex = 20
        Me.btnDestCon.Text = "Destination Consignment"
        Me.btnDestCon.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDestCon.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnDestCon.UseVisualStyleBackColor = False
        '
        'btnDRSReports
        '
        Me.btnDRSReports.BackColor = System.Drawing.SystemColors.Window
        Me.btnDRSReports.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnDRSReports.Image = CType(resources.GetObject("btnDRSReports.Image"), System.Drawing.Image)
        Me.btnDRSReports.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDRSReports.Location = New System.Drawing.Point(13, 48)
        Me.btnDRSReports.Name = "btnDRSReports"
        Me.btnDRSReports.Size = New System.Drawing.Size(289, 23)
        Me.btnDRSReports.TabIndex = 21
        Me.btnDRSReports.Text = "DRS Reports"
        Me.btnDRSReports.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDRSReports.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnDRSReports.UseVisualStyleBackColor = False
        '
        'frmDashboard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(628, 544)
        Me.ControlBox = False
        Me.Controls.Add(Me.userBox2)
        Me.Controls.Add(Me.btnLogout)
        Me.Controls.Add(Me.adminBox3)
        Me.Controls.Add(Me.userBox1)
        Me.Controls.Add(Me.adminBox2)
        Me.Controls.Add(Me.adminBox1)
        Me.Controls.Add(Me.PictureBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmDashboard"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "JRS - Dashboard"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.adminBox1.ResumeLayout(False)
        Me.adminBox2.ResumeLayout(False)
        Me.adminBox3.ResumeLayout(False)
        Me.userBox1.ResumeLayout(False)
        Me.userBox2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents adminBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnTransact As System.Windows.Forms.Button
    Friend WithEvents btnOpenship As System.Windows.Forms.Button
    Friend WithEvents btnNewship As System.Windows.Forms.Button
    Friend WithEvents adminBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents adminBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btnManifest As System.Windows.Forms.Button
    Friend WithEvents btnConsign As System.Windows.Forms.Button
    Friend WithEvents btnDeliver As System.Windows.Forms.Button
    Friend WithEvents btnEntry As System.Windows.Forms.Button
    Friend WithEvents btnLogout As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents userBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents userBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnConsignment As System.Windows.Forms.Button
    Friend WithEvents btnManifest2 As System.Windows.Forms.Button
    Friend WithEvents btnReports As System.Windows.Forms.Button
    Friend WithEvents btnDestCon As System.Windows.Forms.Button
    Friend WithEvents btnDRSReports As System.Windows.Forms.Button
End Class
