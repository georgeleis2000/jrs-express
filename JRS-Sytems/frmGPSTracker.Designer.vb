﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGPSTracker
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.webWindow = New System.Windows.Forms.WebBrowser()
        Me.txtTrackNum = New System.Windows.Forms.TextBox()
        Me.btnGo = New System.Windows.Forms.Button()
        Me.btnHide = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'webWindow
        '
        Me.webWindow.Dock = System.Windows.Forms.DockStyle.Fill
        Me.webWindow.Location = New System.Drawing.Point(0, 0)
        Me.webWindow.MinimumSize = New System.Drawing.Size(20, 20)
        Me.webWindow.Name = "webWindow"
        Me.webWindow.Size = New System.Drawing.Size(521, 427)
        Me.webWindow.TabIndex = 0
        '
        'txtTrackNum
        '
        Me.txtTrackNum.Location = New System.Drawing.Point(13, 13)
        Me.txtTrackNum.Name = "txtTrackNum"
        Me.txtTrackNum.Size = New System.Drawing.Size(263, 20)
        Me.txtTrackNum.TabIndex = 1
        '
        'btnGo
        '
        Me.btnGo.Enabled = False
        Me.btnGo.Location = New System.Drawing.Point(283, 9)
        Me.btnGo.Name = "btnGo"
        Me.btnGo.Size = New System.Drawing.Size(75, 23)
        Me.btnGo.TabIndex = 2
        Me.btnGo.Text = "Go"
        Me.btnGo.UseVisualStyleBackColor = True
        '
        'btnHide
        '
        Me.btnHide.Location = New System.Drawing.Point(422, 392)
        Me.btnHide.Name = "btnHide"
        Me.btnHide.Size = New System.Drawing.Size(75, 23)
        Me.btnHide.TabIndex = 3
        Me.btnHide.Text = "Hide"
        Me.btnHide.UseVisualStyleBackColor = True
        '
        'frmGPSTracker
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(521, 427)
        Me.Controls.Add(Me.btnHide)
        Me.Controls.Add(Me.btnGo)
        Me.Controls.Add(Me.txtTrackNum)
        Me.Controls.Add(Me.webWindow)
        Me.Name = "frmGPSTracker"
        Me.Text = "GPS Tracker"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents webWindow As System.Windows.Forms.WebBrowser
    Friend WithEvents txtTrackNum As System.Windows.Forms.TextBox
    Friend WithEvents btnGo As System.Windows.Forms.Button
    Friend WithEvents btnHide As System.Windows.Forms.Button
End Class
