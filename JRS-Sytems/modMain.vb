﻿Imports MySql.Data.MySqlClient

Module modMain

    Public dbHost = "localhost"
    Public dbUser = "root"
    Public dbPass = ""
    Public db = "gdl_jrs_dev"
    Public is_user_logged_in As Boolean = False
    Public logged_in_user As Integer = 0
    Public is_admin As Boolean = False

    Public Function NewConnection()
        Dim con As New MySqlConnection

        If con.State = ConnectionState.Closed Then
            'this line of code is simply we copy this one from our form
            con.ConnectionString = ("server=" & dbHost & ";user id=" & dbUser & ";password=" & dbPass & ";database=" & db)
            con.Open()
        Else
            MsgBox("Error: 500: Internal Error!")
        End If

        Return con
    End Function

    Public Function get_option(ByVal option_key)
        'MySqlCommand It represents a SQL statement to execute against a MySQL Database
        Dim cmd As New MySqlCommand
        Dim Result As String
        Dim con = NewConnection()

        With cmd
            .Connection = con
            .CommandText = "SELECT value FROM `jrs_options` WHERE `key` = '" & option_key & "'"
            Result = Convert.ToString(cmd.ExecuteScalar())
        End With

        con.Close()

        Return Result
    End Function

    Public Function get_user_by_login(ByVal user_login As String, ByRef column As String)
        Dim cmd As New MySqlCommand
        Dim Result As String = ""
        Dim con = NewConnection()

        If column = "" Then
            column = "user_pass"
        End If

        With cmd
            .Connection = con
            .CommandText = "SELECT " & column & " FROM `jrs_users` WHERE 1 = 1 AND `user_login` = '" & user_login & "'"
            Result = Convert.ToString(cmd.ExecuteScalar())
        End With

        Return Result
    End Function

    Public Function get_user_by_id(ByVal user_id As Integer, ByRef column As String)
        Dim cmd As New MySqlCommand
        Dim Result As String = ""
        Dim con = NewConnection()

        If column = "" Then
            column = "display_name"
        End If

        With cmd
            .Connection = con
            .CommandText = "SELECT " & column & " FROM `jrs_users` WHERE 1 = 1 AND `ID` = '" & user_id & "'"
            Result = Convert.ToString(cmd.ExecuteScalar())
        End With

        Return Result
    End Function

    Public Sub ExitSystem(ByVal e)
        frmMain.Close()
    End Sub

    'This function accepts a string and converts it to its corresponding 32 bit hexadecimal MD5 hash. It comes in handy for encrypting passwords and keys
    Public Function getMD5Hash(ByVal strToHash As String) As String
        Dim md5Obj As New System.Security.Cryptography.MD5CryptoServiceProvider()
        Dim bytesToHash() As Byte = System.Text.Encoding.ASCII.GetBytes(strToHash)

        bytesToHash = md5Obj.ComputeHash(bytesToHash)

        Dim strResult As String = ""
        Dim b As Byte

        For Each b In bytesToHash
            strResult += b.ToString("x2")
        Next

        Return strResult
    End Function

    Public Sub setFocusMDIChild(ByVal currentForm As Form)

        If is_user_logged_in <> False Then
            frmMain.loadDashboard(currentForm)
        Else
            frmMain.LoadLogin()
        End If

    End Sub

End Module
