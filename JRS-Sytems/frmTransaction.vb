﻿Public Class frmTransaction

    Private Sub frmTransaction_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        setFocusMDIChild(Me)
    End Sub

    Private Sub frmTransaction_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Gdl_jrs_devDataSet.Transactions' table. You can move, or remove it, as needed.
        Me.TransactionsTableAdapter.Fill(Me.Gdl_jrs_devDataSet.Transactions)
        frmMain.unloadDashboard(Me)
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        SearchFunc()
    End Sub

    Public Sub SearchFunc()
        Dim rowindex As String
        Dim found As Boolean = False
        For Each row As DataGridViewRow In DataGridView1.Rows
            If row.Cells(1).Value = txtTransCode.Text Then
                rowindex = row.Index.ToString()
                found = True
                ' Dim actie As String = row.Cells(0).Value.ToString()
                ' MsgBox(actie)

                DataGridView1.Rows(rowindex).Selected = True
                Exit For
            End If
        Next
        If Not found Then
            MsgBox("Item not found")
        End If
    End Sub

    Private Sub DataGridView1_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged
        Dim selectedRowCount As Integer = DataGridView1.Rows.GetRowCount(DataGridViewElementStates.Selected)

        If selectedRowCount > 0 Then
            btnUpdate.Enabled = True
        Else
            btnUpdate.Enabled = False
        End If
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim value As Object = DataGridView1.SelectedRows(0).Cells(0).Value.ToString()
        Dim status As Object = DataGridView1.SelectedRows(0).Cells(7).Value.ToString()
        Dim UpdateForm As New frmUpdateTransaction()

        UpdateForm.cboStatus.Focus()
        UpdateForm.txtTrans_ID.Text = value

        If UpdateForm.cboStatus.Items.Contains(status) Then
            UpdateForm.cboStatus.SelectedItem = status
        Else
            UpdateForm.cboStatus.SelectedItem = "Custom"
            UpdateForm.txtCustomStatus.Text = status
        End If

        Dim res As DialogResult = UpdateForm.ShowDialog(Me)

        If (res = DialogResult.Cancel) Then
            'Do nothing
        Else
            Me.TransactionsBindingSource.DataSource = Me.TransactionsTableAdapter.GetData
            Me.TransactionsBindingSource.ResetBindings(False)
            Me.DataGridView1.Refresh()
        End If
    End Sub

End Class