﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNewShipment
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tab1 = New System.Windows.Forms.TabPage()
        Me.txtFromTIN = New System.Windows.Forms.MaskedTextBox()
        Me.txtFromFax = New System.Windows.Forms.MaskedTextBox()
        Me.txtFromMobile = New System.Windows.Forms.MaskedTextBox()
        Me.txtFromPhone = New System.Windows.Forms.MaskedTextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtFromRegion = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtFromZip = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtFromAddress = New System.Windows.Forms.TextBox()
        Me.txtFromCity = New System.Windows.Forms.TextBox()
        Me.txtFromCountry = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtFromName = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.tab2 = New System.Windows.Forms.TabPage()
        Me.txtToTIN = New System.Windows.Forms.MaskedTextBox()
        Me.txtToFax = New System.Windows.Forms.MaskedTextBox()
        Me.txtToMobile = New System.Windows.Forms.MaskedTextBox()
        Me.txtToPhone = New System.Windows.Forms.MaskedTextBox()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtToName = New System.Windows.Forms.TextBox()
        Me.txtToName2 = New System.Windows.Forms.TextBox()
        Me.txtToAddress = New System.Windows.Forms.TextBox()
        Me.txtToCity = New System.Windows.Forms.TextBox()
        Me.txtToCountry = New System.Windows.Forms.TextBox()
        Me.txtToRegion = New System.Windows.Forms.TextBox()
        Me.txtToZip = New System.Windows.Forms.TextBox()
        Me.tab3 = New System.Windows.Forms.TabPage()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.chValuation = New System.Windows.Forms.CheckBox()
        Me.chInsurance = New System.Windows.Forms.CheckBox()
        Me.cboDestination = New System.Windows.Forms.ComboBox()
        Me.txtWeight = New System.Windows.Forms.TextBox()
        Me.txtPieces = New System.Windows.Forms.TextBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.cboOrigin = New System.Windows.Forms.ComboBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.groupPackage = New System.Windows.Forms.GroupBox()
        Me.rdoBoxLarge = New System.Windows.Forms.RadioButton()
        Me.rdoBoxMedium = New System.Windows.Forms.RadioButton()
        Me.rdoBoxSmall = New System.Windows.Forms.RadioButton()
        Me.rdoGeneralCargo = New System.Windows.Forms.RadioButton()
        Me.rdoFivePounder = New System.Windows.Forms.RadioButton()
        Me.rdoThreePounder = New System.Windows.Forms.RadioButton()
        Me.rdoOnePounder = New System.Windows.Forms.RadioButton()
        Me.rdoExpressLetter = New System.Windows.Forms.RadioButton()
        Me.rdoEnvelop = New System.Windows.Forms.RadioButton()
        Me.rdoLegalLetter = New System.Windows.Forms.RadioButton()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.serviceGroup = New System.Windows.Forms.GroupBox()
        Me.rdoOrdinaryType = New System.Windows.Forms.RadioButton()
        Me.rdoExpressType = New System.Windows.Forms.RadioButton()
        Me.tab4 = New System.Windows.Forms.TabPage()
        Me.txtConsignmentNote = New System.Windows.Forms.TextBox()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.txtTotal = New System.Windows.Forms.TextBox()
        Me.txtOther = New System.Windows.Forms.TextBox()
        Me.txtPickupFee = New System.Windows.Forms.TextBox()
        Me.txtInsurance = New System.Windows.Forms.TextBox()
        Me.txtValuation = New System.Windows.Forms.TextBox()
        Me.txtFreight = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.txtChange = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.txtAmountDue = New System.Windows.Forms.TextBox()
        Me.txtVat = New System.Windows.Forms.TextBox()
        Me.txtCash = New System.Windows.Forms.TextBox()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.TabControl1.SuspendLayout()
        Me.tab1.SuspendLayout()
        Me.tab2.SuspendLayout()
        Me.tab3.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.groupPackage.SuspendLayout()
        Me.serviceGroup.SuspendLayout()
        Me.tab4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.tab1)
        Me.TabControl1.Controls.Add(Me.tab2)
        Me.TabControl1.Controls.Add(Me.tab3)
        Me.TabControl1.Controls.Add(Me.tab4)
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(382, 466)
        Me.TabControl1.TabIndex = 0
        '
        'tab1
        '
        Me.tab1.Controls.Add(Me.txtFromTIN)
        Me.tab1.Controls.Add(Me.txtFromFax)
        Me.tab1.Controls.Add(Me.txtFromMobile)
        Me.tab1.Controls.Add(Me.txtFromPhone)
        Me.tab1.Controls.Add(Me.Label1)
        Me.tab1.Controls.Add(Me.Label20)
        Me.tab1.Controls.Add(Me.txtFromRegion)
        Me.tab1.Controls.Add(Me.Label14)
        Me.tab1.Controls.Add(Me.txtFromZip)
        Me.tab1.Controls.Add(Me.Label13)
        Me.tab1.Controls.Add(Me.txtFromAddress)
        Me.tab1.Controls.Add(Me.txtFromCity)
        Me.tab1.Controls.Add(Me.txtFromCountry)
        Me.tab1.Controls.Add(Me.Label19)
        Me.tab1.Controls.Add(Me.Label18)
        Me.tab1.Controls.Add(Me.Label17)
        Me.tab1.Controls.Add(Me.Label16)
        Me.tab1.Controls.Add(Me.txtFromName)
        Me.tab1.Controls.Add(Me.Label15)
        Me.tab1.Controls.Add(Me.Label12)
        Me.tab1.Location = New System.Drawing.Point(4, 22)
        Me.tab1.Name = "tab1"
        Me.tab1.Padding = New System.Windows.Forms.Padding(3)
        Me.tab1.Size = New System.Drawing.Size(374, 440)
        Me.tab1.TabIndex = 1
        Me.tab1.Text = "Ship From"
        Me.tab1.UseVisualStyleBackColor = True
        '
        'txtFromTIN
        '
        Me.txtFromTIN.Location = New System.Drawing.Point(190, 282)
        Me.txtFromTIN.Mask = "000-000-000-000"
        Me.txtFromTIN.Name = "txtFromTIN"
        Me.txtFromTIN.Size = New System.Drawing.Size(150, 20)
        Me.txtFromTIN.TabIndex = 9
        '
        'txtFromFax
        '
        Me.txtFromFax.Location = New System.Drawing.Point(18, 282)
        Me.txtFromFax.Mask = "(999) 000-0000"
        Me.txtFromFax.Name = "txtFromFax"
        Me.txtFromFax.Size = New System.Drawing.Size(149, 20)
        Me.txtFromFax.TabIndex = 8
        '
        'txtFromMobile
        '
        Me.txtFromMobile.Location = New System.Drawing.Point(190, 233)
        Me.txtFromMobile.Mask = "+63 999-000-0000"
        Me.txtFromMobile.Name = "txtFromMobile"
        Me.txtFromMobile.Size = New System.Drawing.Size(150, 20)
        Me.txtFromMobile.TabIndex = 7
        '
        'txtFromPhone
        '
        Me.txtFromPhone.Location = New System.Drawing.Point(18, 233)
        Me.txtFromPhone.Mask = "(999) 000-0000"
        Me.txtFromPhone.Name = "txtFromPhone"
        Me.txtFromPhone.Size = New System.Drawing.Size(149, 20)
        Me.txtFromPhone.TabIndex = 6
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(187, 216)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 13)
        Me.Label1.TabIndex = 26
        Me.Label1.Text = "Mobile No.:"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(15, 266)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(53, 13)
        Me.Label20.TabIndex = 25
        Me.Label20.Text = "Facsimile:"
        '
        'txtFromRegion
        '
        Me.txtFromRegion.Location = New System.Drawing.Point(18, 181)
        Me.txtFromRegion.Name = "txtFromRegion"
        Me.txtFromRegion.Size = New System.Drawing.Size(157, 20)
        Me.txtFromRegion.TabIndex = 4
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(15, 165)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(44, 13)
        Me.Label14.TabIndex = 22
        Me.Label14.Text = "Region:"
        '
        'txtFromZip
        '
        Me.txtFromZip.Location = New System.Drawing.Point(190, 133)
        Me.txtFromZip.MaxLength = 5
        Me.txtFromZip.Name = "txtFromZip"
        Me.txtFromZip.Size = New System.Drawing.Size(154, 20)
        Me.txtFromZip.TabIndex = 3
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(187, 117)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(53, 13)
        Me.Label13.TabIndex = 20
        Me.Label13.Text = "Zip Code:"
        '
        'txtFromAddress
        '
        Me.txtFromAddress.Location = New System.Drawing.Point(18, 82)
        Me.txtFromAddress.Name = "txtFromAddress"
        Me.txtFromAddress.Size = New System.Drawing.Size(326, 20)
        Me.txtFromAddress.TabIndex = 1
        '
        'txtFromCity
        '
        Me.txtFromCity.Location = New System.Drawing.Point(18, 133)
        Me.txtFromCity.Name = "txtFromCity"
        Me.txtFromCity.Size = New System.Drawing.Size(157, 20)
        Me.txtFromCity.TabIndex = 2
        '
        'txtFromCountry
        '
        Me.txtFromCountry.Location = New System.Drawing.Point(190, 181)
        Me.txtFromCountry.Name = "txtFromCountry"
        Me.txtFromCountry.Size = New System.Drawing.Size(154, 20)
        Me.txtFromCountry.TabIndex = 5
        Me.txtFromCountry.Text = "Philippines"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(187, 266)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(31, 13)
        Me.Label19.TabIndex = 8
        Me.Label19.Text = "TIN :"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(15, 216)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(70, 13)
        Me.Label18.TabIndex = 7
        Me.Label18.Text = "Contact No. :"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(187, 165)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(49, 13)
        Me.Label17.TabIndex = 6
        Me.Label17.Text = "Country :"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(18, 117)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(30, 13)
        Me.Label16.TabIndex = 5
        Me.Label16.Text = "City :"
        '
        'txtFromName
        '
        Me.txtFromName.Location = New System.Drawing.Point(18, 36)
        Me.txtFromName.Name = "txtFromName"
        Me.txtFromName.Size = New System.Drawing.Size(326, 20)
        Me.txtFromName.TabIndex = 0
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(18, 66)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(145, 13)
        Me.Label15.TabIndex = 3
        Me.Label15.Text = "Room/Floor/Street/Address :"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(18, 20)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(38, 13)
        Me.Label12.TabIndex = 0
        Me.Label12.Text = "Name:"
        '
        'tab2
        '
        Me.tab2.Controls.Add(Me.txtToTIN)
        Me.tab2.Controls.Add(Me.txtToFax)
        Me.tab2.Controls.Add(Me.txtToMobile)
        Me.tab2.Controls.Add(Me.txtToPhone)
        Me.tab2.Controls.Add(Me.Label44)
        Me.tab2.Controls.Add(Me.Label11)
        Me.tab2.Controls.Add(Me.Label10)
        Me.tab2.Controls.Add(Me.Label9)
        Me.tab2.Controls.Add(Me.Label8)
        Me.tab2.Controls.Add(Me.Label7)
        Me.tab2.Controls.Add(Me.Label6)
        Me.tab2.Controls.Add(Me.Label5)
        Me.tab2.Controls.Add(Me.Label4)
        Me.tab2.Controls.Add(Me.Label3)
        Me.tab2.Controls.Add(Me.Label2)
        Me.tab2.Controls.Add(Me.txtToName)
        Me.tab2.Controls.Add(Me.txtToName2)
        Me.tab2.Controls.Add(Me.txtToAddress)
        Me.tab2.Controls.Add(Me.txtToCity)
        Me.tab2.Controls.Add(Me.txtToCountry)
        Me.tab2.Controls.Add(Me.txtToRegion)
        Me.tab2.Controls.Add(Me.txtToZip)
        Me.tab2.Location = New System.Drawing.Point(4, 22)
        Me.tab2.Name = "tab2"
        Me.tab2.Padding = New System.Windows.Forms.Padding(3)
        Me.tab2.Size = New System.Drawing.Size(374, 440)
        Me.tab2.TabIndex = 0
        Me.tab2.Text = "Ship To"
        Me.tab2.UseVisualStyleBackColor = True
        '
        'txtToTIN
        '
        Me.txtToTIN.Location = New System.Drawing.Point(198, 297)
        Me.txtToTIN.Mask = "000-000-000-000"
        Me.txtToTIN.Name = "txtToTIN"
        Me.txtToTIN.Size = New System.Drawing.Size(154, 20)
        Me.txtToTIN.TabIndex = 20
        '
        'txtToFax
        '
        Me.txtToFax.Location = New System.Drawing.Point(17, 297)
        Me.txtToFax.Mask = "(999) 000-0000"
        Me.txtToFax.Name = "txtToFax"
        Me.txtToFax.Size = New System.Drawing.Size(154, 20)
        Me.txtToFax.TabIndex = 19
        '
        'txtToMobile
        '
        Me.txtToMobile.Location = New System.Drawing.Point(198, 249)
        Me.txtToMobile.Mask = "+63 999-000-0000"
        Me.txtToMobile.Name = "txtToMobile"
        Me.txtToMobile.Size = New System.Drawing.Size(154, 20)
        Me.txtToMobile.TabIndex = 18
        '
        'txtToPhone
        '
        Me.txtToPhone.Location = New System.Drawing.Point(17, 249)
        Me.txtToPhone.Mask = "(999) 000-0000"
        Me.txtToPhone.Name = "txtToPhone"
        Me.txtToPhone.Size = New System.Drawing.Size(154, 20)
        Me.txtToPhone.TabIndex = 17
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(195, 229)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(61, 13)
        Me.Label44.TabIndex = 26
        Me.Label44.Text = "Mobile No.:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(195, 280)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(28, 13)
        Me.Label11.TabIndex = 24
        Me.Label11.Text = "TIN:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(14, 280)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(53, 13)
        Me.Label10.TabIndex = 23
        Me.Label10.Text = "Facsimile:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(14, 229)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(67, 13)
        Me.Label9.TabIndex = 22
        Me.Label9.Text = "Contact No.:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(14, 189)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(44, 13)
        Me.Label8.TabIndex = 21
        Me.Label8.Text = "Region:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(195, 189)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(46, 13)
        Me.Label7.TabIndex = 20
        Me.Label7.Text = "Country:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(195, 145)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(53, 13)
        Me.Label6.TabIndex = 19
        Me.Label6.Text = "Zip Code:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(14, 145)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(27, 13)
        Me.Label5.TabIndex = 18
        Me.Label5.Text = "City:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(14, 101)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(142, 13)
        Me.Label4.TabIndex = 17
        Me.Label4.Text = "Room/Floor/Street/Address:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(14, 58)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(52, 13)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "Attention:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(14, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(38, 13)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "Name:"
        '
        'txtToName
        '
        Me.txtToName.Location = New System.Drawing.Point(17, 33)
        Me.txtToName.Name = "txtToName"
        Me.txtToName.Size = New System.Drawing.Size(335, 20)
        Me.txtToName.TabIndex = 10
        '
        'txtToName2
        '
        Me.txtToName2.Location = New System.Drawing.Point(17, 74)
        Me.txtToName2.Name = "txtToName2"
        Me.txtToName2.Size = New System.Drawing.Size(335, 20)
        Me.txtToName2.TabIndex = 11
        '
        'txtToAddress
        '
        Me.txtToAddress.Location = New System.Drawing.Point(17, 117)
        Me.txtToAddress.Name = "txtToAddress"
        Me.txtToAddress.Size = New System.Drawing.Size(335, 20)
        Me.txtToAddress.TabIndex = 12
        '
        'txtToCity
        '
        Me.txtToCity.Location = New System.Drawing.Point(17, 161)
        Me.txtToCity.Name = "txtToCity"
        Me.txtToCity.Size = New System.Drawing.Size(154, 20)
        Me.txtToCity.TabIndex = 13
        '
        'txtToCountry
        '
        Me.txtToCountry.Location = New System.Drawing.Point(198, 205)
        Me.txtToCountry.Name = "txtToCountry"
        Me.txtToCountry.Size = New System.Drawing.Size(154, 20)
        Me.txtToCountry.TabIndex = 16
        Me.txtToCountry.Text = "Philippines"
        '
        'txtToRegion
        '
        Me.txtToRegion.Location = New System.Drawing.Point(17, 205)
        Me.txtToRegion.Name = "txtToRegion"
        Me.txtToRegion.Size = New System.Drawing.Size(154, 20)
        Me.txtToRegion.TabIndex = 15
        '
        'txtToZip
        '
        Me.txtToZip.Location = New System.Drawing.Point(198, 161)
        Me.txtToZip.MaxLength = 5
        Me.txtToZip.Name = "txtToZip"
        Me.txtToZip.Size = New System.Drawing.Size(154, 20)
        Me.txtToZip.TabIndex = 14
        '
        'tab3
        '
        Me.tab3.Controls.Add(Me.GroupBox3)
        Me.tab3.Controls.Add(Me.groupPackage)
        Me.tab3.Controls.Add(Me.serviceGroup)
        Me.tab3.Location = New System.Drawing.Point(4, 22)
        Me.tab3.Name = "tab3"
        Me.tab3.Size = New System.Drawing.Size(374, 440)
        Me.tab3.TabIndex = 2
        Me.tab3.Text = "Options"
        Me.tab3.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label33)
        Me.GroupBox3.Controls.Add(Me.chValuation)
        Me.GroupBox3.Controls.Add(Me.chInsurance)
        Me.GroupBox3.Controls.Add(Me.cboDestination)
        Me.GroupBox3.Controls.Add(Me.txtWeight)
        Me.GroupBox3.Controls.Add(Me.txtPieces)
        Me.GroupBox3.Controls.Add(Me.Label32)
        Me.GroupBox3.Controls.Add(Me.cboOrigin)
        Me.GroupBox3.Controls.Add(Me.Label22)
        Me.GroupBox3.Controls.Add(Me.Label21)
        Me.GroupBox3.Location = New System.Drawing.Point(17, 68)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(341, 146)
        Me.GroupBox3.TabIndex = 10
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Package Specification"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(164, 22)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(63, 13)
        Me.Label33.TabIndex = 50
        Me.Label33.Text = "Destination:"
        '
        'chValuation
        '
        Me.chValuation.AutoSize = True
        Me.chValuation.Location = New System.Drawing.Point(167, 104)
        Me.chValuation.Name = "chValuation"
        Me.chValuation.Size = New System.Drawing.Size(147, 17)
        Me.chValuation.TabIndex = 26
        Me.chValuation.Text = "Valuation (In case of loss)"
        Me.chValuation.UseVisualStyleBackColor = True
        '
        'chInsurance
        '
        Me.chInsurance.AutoSize = True
        Me.chInsurance.Location = New System.Drawing.Point(167, 82)
        Me.chInsurance.Name = "chInsurance"
        Me.chInsurance.Size = New System.Drawing.Size(169, 17)
        Me.chInsurance.TabIndex = 25
        Me.chInsurance.Text = "Insurance (in case of damage)"
        Me.chInsurance.UseVisualStyleBackColor = True
        '
        'cboDestination
        '
        Me.cboDestination.AutoCompleteCustomSource.AddRange(New String() {"Abra", "Agusan del Norte", "Agusan del Sur", "Aklan", "Albay", "Antique", "Apayao", "Aurora", "Basilan", "Bataan", "Batanes", "Batangas", "Benguet", "Biliran", "Bohol", "Bukidnon", "Bulacan", "Cagayan", "Camarines Norte", "Camarines Sur", "Camiguin", "Capiz", "Catanduanes", "Cavite", "Cebu", "Compostela Valley", "Davao del Norte", "Davao del Sur", "Davao Oriental", "Eastern Samar", "Guimaras", "Ifugao", "Ilocos Norte", "Ilocos Sur", "Iloilo", "Isabela", "Kalinga", "La Union", "Laguna", "Lanao del Norte", "Lanao del Sur", "Leyte", "Maguindanao", "Marinduque", "Masbate", "Metro Manila", "Mindoro Occidental", "Mindoro Oriental", "Misamis Occidental", "Misamis Oriental", "Mountain Province", "Negros Occidental", "Negros Oriental", "North Cotabato", "Northern Samar", "Nueva Ecija", "Nueva Vizcaya", "Palawan", "Pampanga", "Pangasinan", "Quezon", "Quirino", "Rizal", "Romblon", "Samar", "Sarangani", "Siquijor", "Sorsogon", "South Cotabato", "Southern Leyte", "Sultan Kudarat", "Sulu", "Surigao del Norte", "Surigao del Sur", "Tarlac", "Tawi-Tawi", "Zambales", "Zamboanga del Norte", "Zamboanga del Sur", "Zamboanga Sibugay"})
        Me.cboDestination.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDestination.FormattingEnabled = True
        Me.cboDestination.Items.AddRange(New Object() {"Abra", "Agusan del Norte", "Agusan del Sur", "Aklan", "Albay", "Antique", "Apayao", "Aurora", "Basilan", "Bataan", "Batanes", "Batangas", "Benguet", "Biliran", "Bohol", "Bukidnon", "Bulacan", "Cagayan", "Camarines Norte", "Camarines Sur", "Camiguin", "Capiz", "Catanduanes", "Cavite", "Cebu", "Compostela Valley", "Davao del Norte", "Davao del Sur", "Davao Oriental", "Eastern Samar", "Guimaras", "Ifugao", "Ilocos Norte", "Ilocos Sur", "Iloilo", "Isabela", "Kalinga", "La Union", "Laguna", "Lanao del Norte", "Lanao del Sur", "Leyte", "Maguindanao", "Marinduque", "Masbate", "Metro Manila", "Mindoro Occidental", "Mindoro Oriental", "Misamis Occidental", "Misamis Oriental", "Mountain Province", "Negros Occidental", "Negros Oriental", "North Cotabato", "Northern Samar", "Nueva Ecija", "Nueva Vizcaya", "Palawan", "Pampanga", "Pangasinan", "Quezon", "Quirino", "Rizal", "Romblon", "Samar", "Sarangani", "Siquijor", "Sorsogon", "South Cotabato", "Southern Leyte", "Sultan Kudarat", "Sulu", "Surigao del Norte", "Surigao del Sur", "Tarlac", "Tawi-Tawi", "Zambales", "Zamboanga del Norte", "Zamboanga del Sur", "Zamboanga Sibugay"})
        Me.cboDestination.Location = New System.Drawing.Point(167, 41)
        Me.cboDestination.Name = "cboDestination"
        Me.cboDestination.Size = New System.Drawing.Size(166, 21)
        Me.cboDestination.Sorted = True
        Me.cboDestination.TabIndex = 49
        '
        'txtWeight
        '
        Me.txtWeight.Location = New System.Drawing.Point(96, 105)
        Me.txtWeight.Name = "txtWeight"
        Me.txtWeight.Size = New System.Drawing.Size(65, 20)
        Me.txtWeight.TabIndex = 24
        Me.txtWeight.Text = "0"
        '
        'txtPieces
        '
        Me.txtPieces.Location = New System.Drawing.Point(96, 77)
        Me.txtPieces.Name = "txtPieces"
        Me.txtPieces.Size = New System.Drawing.Size(65, 20)
        Me.txtPieces.TabIndex = 23
        Me.txtPieces.Text = "0"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(9, 25)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(37, 13)
        Me.Label32.TabIndex = 48
        Me.Label32.Text = "Origin:"
        '
        'cboOrigin
        '
        Me.cboOrigin.AutoCompleteCustomSource.AddRange(New String() {"Abra", "Agusan del Norte", "Agusan del Sur", "Aklan", "Albay", "Antique", "Apayao", "Aurora", "Basilan", "Bataan", "Batanes", "Batangas", "Benguet", "Biliran", "Bohol", "Bukidnon", "Bulacan", "Cagayan", "Camarines Norte", "Camarines Sur", "Camiguin", "Capiz", "Catanduanes", "Cavite", "Cebu", "Compostela Valley", "Davao del Norte", "Davao del Sur", "Davao Oriental", "Eastern Samar", "Guimaras", "Ifugao", "Ilocos Norte", "Ilocos Sur", "Iloilo", "Isabela", "Kalinga", "La Union", "Laguna", "Lanao del Norte", "Lanao del Sur", "Leyte", "Maguindanao", "Marinduque", "Masbate", "Metro Manila", "Mindoro Occidental", "Mindoro Oriental", "Misamis Occidental", "Misamis Oriental", "Mountain Province", "Negros Occidental", "Negros Oriental", "North Cotabato", "Northern Samar", "Nueva Ecija", "Nueva Vizcaya", "Palawan", "Pampanga", "Pangasinan", "Quezon", "Quirino", "Rizal", "Romblon", "Samar", "Sarangani", "Siquijor", "Sorsogon", "South Cotabato", "Southern Leyte", "Sultan Kudarat", "Sulu", "Surigao del Norte", "Surigao del Sur", "Tarlac", "Tawi-Tawi", "Zambales", "Zamboanga del Norte", "Zamboanga del Sur", "Zamboanga Sibugay"})
        Me.cboOrigin.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOrigin.FormattingEnabled = True
        Me.cboOrigin.Items.AddRange(New Object() {"Abra", "Agusan del Norte", "Agusan del Sur", "Aklan", "Albay", "Antique", "Apayao", "Aurora", "Basilan", "Bataan", "Batanes", "Batangas", "Benguet", "Biliran", "Bohol", "Bukidnon", "Bulacan", "Cagayan", "Camarines Norte", "Camarines Sur", "Camiguin", "Capiz", "Catanduanes", "Cavite", "Cebu", "Compostela Valley", "Davao del Norte", "Davao del Sur", "Davao Oriental", "Eastern Samar", "Guimaras", "Ifugao", "Ilocos Norte", "Ilocos Sur", "Iloilo", "Isabela", "Kalinga", "La Union", "Laguna", "Lanao del Norte", "Lanao del Sur", "Leyte", "Maguindanao", "Marinduque", "Masbate", "Metro Manila", "Mindoro Occidental", "Mindoro Oriental", "Misamis Occidental", "Misamis Oriental", "Mountain Province", "Negros Occidental", "Negros Oriental", "North Cotabato", "Northern Samar", "Nueva Ecija", "Nueva Vizcaya", "Palawan", "Pampanga", "Pangasinan", "Quezon", "Quirino", "Rizal", "Romblon", "Samar", "Sarangani", "Siquijor", "Sorsogon", "South Cotabato", "Southern Leyte", "Sultan Kudarat", "Sulu", "Surigao del Norte", "Surigao del Sur", "Tarlac", "Tawi-Tawi", "Zambales", "Zamboanga del Norte", "Zamboanga del Sur", "Zamboanga Sibugay"})
        Me.cboOrigin.Location = New System.Drawing.Point(12, 41)
        Me.cboOrigin.Name = "cboOrigin"
        Me.cboOrigin.Size = New System.Drawing.Size(149, 21)
        Me.cboOrigin.Sorted = True
        Me.cboOrigin.TabIndex = 47
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(9, 108)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(81, 13)
        Me.Label22.TabIndex = 1
        Me.Label22.Text = "Weight (grams):"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(9, 80)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(42, 13)
        Me.Label21.TabIndex = 0
        Me.Label21.Text = "Pieces:"
        '
        'groupPackage
        '
        Me.groupPackage.Controls.Add(Me.rdoBoxLarge)
        Me.groupPackage.Controls.Add(Me.rdoBoxMedium)
        Me.groupPackage.Controls.Add(Me.rdoBoxSmall)
        Me.groupPackage.Controls.Add(Me.rdoGeneralCargo)
        Me.groupPackage.Controls.Add(Me.rdoFivePounder)
        Me.groupPackage.Controls.Add(Me.rdoThreePounder)
        Me.groupPackage.Controls.Add(Me.rdoOnePounder)
        Me.groupPackage.Controls.Add(Me.rdoExpressLetter)
        Me.groupPackage.Controls.Add(Me.rdoEnvelop)
        Me.groupPackage.Controls.Add(Me.rdoLegalLetter)
        Me.groupPackage.Controls.Add(Me.Label31)
        Me.groupPackage.Controls.Add(Me.Label30)
        Me.groupPackage.Location = New System.Drawing.Point(17, 220)
        Me.groupPackage.Name = "groupPackage"
        Me.groupPackage.Size = New System.Drawing.Size(341, 195)
        Me.groupPackage.TabIndex = 9
        Me.groupPackage.TabStop = False
        Me.groupPackage.Text = "Package Type"
        '
        'rdoBoxLarge
        '
        Me.rdoBoxLarge.AutoSize = True
        Me.rdoBoxLarge.Enabled = False
        Me.rdoBoxLarge.Location = New System.Drawing.Point(177, 120)
        Me.rdoBoxLarge.Name = "rdoBoxLarge"
        Me.rdoBoxLarge.Size = New System.Drawing.Size(113, 17)
        Me.rdoBoxLarge.TabIndex = 46
        Me.rdoBoxLarge.TabStop = True
        Me.rdoBoxLarge.Text = "Express Box Large"
        Me.rdoBoxLarge.UseVisualStyleBackColor = True
        '
        'rdoBoxMedium
        '
        Me.rdoBoxMedium.AutoSize = True
        Me.rdoBoxMedium.Enabled = False
        Me.rdoBoxMedium.Location = New System.Drawing.Point(177, 97)
        Me.rdoBoxMedium.Name = "rdoBoxMedium"
        Me.rdoBoxMedium.Size = New System.Drawing.Size(123, 17)
        Me.rdoBoxMedium.TabIndex = 45
        Me.rdoBoxMedium.TabStop = True
        Me.rdoBoxMedium.Text = "Express Box Medium"
        Me.rdoBoxMedium.UseVisualStyleBackColor = True
        '
        'rdoBoxSmall
        '
        Me.rdoBoxSmall.AutoSize = True
        Me.rdoBoxSmall.Enabled = False
        Me.rdoBoxSmall.Location = New System.Drawing.Point(177, 74)
        Me.rdoBoxSmall.Name = "rdoBoxSmall"
        Me.rdoBoxSmall.Size = New System.Drawing.Size(111, 17)
        Me.rdoBoxSmall.TabIndex = 44
        Me.rdoBoxSmall.TabStop = True
        Me.rdoBoxSmall.Text = "Express Box Small"
        Me.rdoBoxSmall.UseVisualStyleBackColor = True
        '
        'rdoGeneralCargo
        '
        Me.rdoGeneralCargo.AutoSize = True
        Me.rdoGeneralCargo.Location = New System.Drawing.Point(177, 51)
        Me.rdoGeneralCargo.Name = "rdoGeneralCargo"
        Me.rdoGeneralCargo.Size = New System.Drawing.Size(93, 17)
        Me.rdoGeneralCargo.TabIndex = 43
        Me.rdoGeneralCargo.TabStop = True
        Me.rdoGeneralCargo.Text = "General Cargo"
        Me.rdoGeneralCargo.UseVisualStyleBackColor = True
        '
        'rdoFivePounder
        '
        Me.rdoFivePounder.AutoSize = True
        Me.rdoFivePounder.Enabled = False
        Me.rdoFivePounder.Location = New System.Drawing.Point(14, 166)
        Me.rdoFivePounder.Name = "rdoFivePounder"
        Me.rdoFivePounder.Size = New System.Drawing.Size(88, 17)
        Me.rdoFivePounder.TabIndex = 42
        Me.rdoFivePounder.TabStop = True
        Me.rdoFivePounder.Text = "Five Pounder"
        Me.rdoFivePounder.UseVisualStyleBackColor = True
        '
        'rdoThreePounder
        '
        Me.rdoThreePounder.AutoSize = True
        Me.rdoThreePounder.Enabled = False
        Me.rdoThreePounder.Location = New System.Drawing.Point(14, 143)
        Me.rdoThreePounder.Name = "rdoThreePounder"
        Me.rdoThreePounder.Size = New System.Drawing.Size(96, 17)
        Me.rdoThreePounder.TabIndex = 41
        Me.rdoThreePounder.TabStop = True
        Me.rdoThreePounder.Text = "Three Pounder"
        Me.rdoThreePounder.UseVisualStyleBackColor = True
        '
        'rdoOnePounder
        '
        Me.rdoOnePounder.AutoSize = True
        Me.rdoOnePounder.Enabled = False
        Me.rdoOnePounder.Location = New System.Drawing.Point(14, 120)
        Me.rdoOnePounder.Name = "rdoOnePounder"
        Me.rdoOnePounder.Size = New System.Drawing.Size(88, 17)
        Me.rdoOnePounder.TabIndex = 40
        Me.rdoOnePounder.TabStop = True
        Me.rdoOnePounder.Text = "One Pounder"
        Me.rdoOnePounder.UseVisualStyleBackColor = True
        '
        'rdoExpressLetter
        '
        Me.rdoExpressLetter.AutoSize = True
        Me.rdoExpressLetter.Enabled = False
        Me.rdoExpressLetter.Location = New System.Drawing.Point(14, 97)
        Me.rdoExpressLetter.Name = "rdoExpressLetter"
        Me.rdoExpressLetter.Size = New System.Drawing.Size(121, 17)
        Me.rdoExpressLetter.TabIndex = 39
        Me.rdoExpressLetter.TabStop = True
        Me.rdoExpressLetter.Text = "Express Legal Letter"
        Me.rdoExpressLetter.UseVisualStyleBackColor = True
        '
        'rdoEnvelop
        '
        Me.rdoEnvelop.AutoSize = True
        Me.rdoEnvelop.Location = New System.Drawing.Point(14, 74)
        Me.rdoEnvelop.Name = "rdoEnvelop"
        Me.rdoEnvelop.Size = New System.Drawing.Size(127, 17)
        Me.rdoEnvelop.TabIndex = 38
        Me.rdoEnvelop.TabStop = True
        Me.rdoEnvelop.Text = "Brown Envelop Letter"
        Me.rdoEnvelop.UseVisualStyleBackColor = True
        '
        'rdoLegalLetter
        '
        Me.rdoLegalLetter.AutoSize = True
        Me.rdoLegalLetter.Checked = True
        Me.rdoLegalLetter.Location = New System.Drawing.Point(14, 51)
        Me.rdoLegalLetter.Name = "rdoLegalLetter"
        Me.rdoLegalLetter.Size = New System.Drawing.Size(104, 17)
        Me.rdoLegalLetter.TabIndex = 37
        Me.rdoLegalLetter.TabStop = True
        Me.rdoLegalLetter.Text = "Legal Size Letter"
        Me.rdoLegalLetter.UseVisualStyleBackColor = True
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(173, 28)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(58, 20)
        Me.Label31.TabIndex = 36
        Me.Label31.Text = "Boxes"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(10, 28)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(41, 20)
        Me.Label30.TabIndex = 35
        Me.Label30.Text = "Mail"
        '
        'serviceGroup
        '
        Me.serviceGroup.Controls.Add(Me.rdoOrdinaryType)
        Me.serviceGroup.Controls.Add(Me.rdoExpressType)
        Me.serviceGroup.Location = New System.Drawing.Point(17, 14)
        Me.serviceGroup.Name = "serviceGroup"
        Me.serviceGroup.Size = New System.Drawing.Size(341, 48)
        Me.serviceGroup.TabIndex = 8
        Me.serviceGroup.TabStop = False
        Me.serviceGroup.Text = "Service Type"
        '
        'rdoOrdinaryType
        '
        Me.rdoOrdinaryType.AutoSize = True
        Me.rdoOrdinaryType.Checked = True
        Me.rdoOrdinaryType.Location = New System.Drawing.Point(14, 21)
        Me.rdoOrdinaryType.Name = "rdoOrdinaryType"
        Me.rdoOrdinaryType.Size = New System.Drawing.Size(82, 17)
        Me.rdoOrdinaryType.TabIndex = 21
        Me.rdoOrdinaryType.TabStop = True
        Me.rdoOrdinaryType.Text = "ORDINARY"
        Me.rdoOrdinaryType.UseVisualStyleBackColor = True
        '
        'rdoExpressType
        '
        Me.rdoExpressType.AutoSize = True
        Me.rdoExpressType.Location = New System.Drawing.Point(122, 21)
        Me.rdoExpressType.Name = "rdoExpressType"
        Me.rdoExpressType.Size = New System.Drawing.Size(75, 17)
        Me.rdoExpressType.TabIndex = 22
        Me.rdoExpressType.Text = "EXPRESS"
        Me.rdoExpressType.UseVisualStyleBackColor = True
        '
        'tab4
        '
        Me.tab4.Controls.Add(Me.txtConsignmentNote)
        Me.tab4.Controls.Add(Me.Label45)
        Me.tab4.Location = New System.Drawing.Point(4, 22)
        Me.tab4.Name = "tab4"
        Me.tab4.Size = New System.Drawing.Size(374, 440)
        Me.tab4.TabIndex = 3
        Me.tab4.Text = "Details"
        Me.tab4.UseVisualStyleBackColor = True
        '
        'txtConsignmentNote
        '
        Me.txtConsignmentNote.Location = New System.Drawing.Point(16, 33)
        Me.txtConsignmentNote.Multiline = True
        Me.txtConsignmentNote.Name = "txtConsignmentNote"
        Me.txtConsignmentNote.Size = New System.Drawing.Size(341, 387)
        Me.txtConsignmentNote.TabIndex = 35
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(13, 16)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(94, 13)
        Me.Label45.TabIndex = 0
        Me.Label45.Text = "Consignment Note"
        '
        'txtTotal
        '
        Me.txtTotal.BackColor = System.Drawing.Color.White
        Me.txtTotal.Location = New System.Drawing.Point(206, 167)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.ReadOnly = True
        Me.txtTotal.Size = New System.Drawing.Size(100, 20)
        Me.txtTotal.TabIndex = 11
        Me.txtTotal.TabStop = False
        Me.txtTotal.Text = "0.00"
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtOther
        '
        Me.txtOther.BackColor = System.Drawing.Color.White
        Me.txtOther.Location = New System.Drawing.Point(206, 144)
        Me.txtOther.Name = "txtOther"
        Me.txtOther.ReadOnly = True
        Me.txtOther.Size = New System.Drawing.Size(100, 20)
        Me.txtOther.TabIndex = 10
        Me.txtOther.TabStop = False
        Me.txtOther.Text = "0.00"
        Me.txtOther.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPickupFee
        '
        Me.txtPickupFee.BackColor = System.Drawing.Color.White
        Me.txtPickupFee.Location = New System.Drawing.Point(206, 122)
        Me.txtPickupFee.Name = "txtPickupFee"
        Me.txtPickupFee.ReadOnly = True
        Me.txtPickupFee.Size = New System.Drawing.Size(100, 20)
        Me.txtPickupFee.TabIndex = 9
        Me.txtPickupFee.TabStop = False
        Me.txtPickupFee.Text = "0.00"
        Me.txtPickupFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtInsurance
        '
        Me.txtInsurance.BackColor = System.Drawing.Color.White
        Me.txtInsurance.Location = New System.Drawing.Point(206, 98)
        Me.txtInsurance.Name = "txtInsurance"
        Me.txtInsurance.ReadOnly = True
        Me.txtInsurance.Size = New System.Drawing.Size(100, 20)
        Me.txtInsurance.TabIndex = 8
        Me.txtInsurance.TabStop = False
        Me.txtInsurance.Text = "0.00"
        Me.txtInsurance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtValuation
        '
        Me.txtValuation.BackColor = System.Drawing.Color.White
        Me.txtValuation.Location = New System.Drawing.Point(206, 75)
        Me.txtValuation.Name = "txtValuation"
        Me.txtValuation.ReadOnly = True
        Me.txtValuation.Size = New System.Drawing.Size(100, 20)
        Me.txtValuation.TabIndex = 7
        Me.txtValuation.TabStop = False
        Me.txtValuation.Text = "0.00"
        Me.txtValuation.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtFreight
        '
        Me.txtFreight.BackColor = System.Drawing.Color.White
        Me.txtFreight.Location = New System.Drawing.Point(206, 51)
        Me.txtFreight.Name = "txtFreight"
        Me.txtFreight.ReadOnly = True
        Me.txtFreight.Size = New System.Drawing.Size(100, 20)
        Me.txtFreight.TabIndex = 6
        Me.txtFreight.TabStop = False
        Me.txtFreight.Text = "0.00"
        Me.txtFreight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label28
        '
        Me.Label28.BackColor = System.Drawing.Color.White
        Me.Label28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label28.Location = New System.Drawing.Point(15, 165)
        Me.Label28.Name = "Label28"
        Me.Label28.Padding = New System.Windows.Forms.Padding(3)
        Me.Label28.Size = New System.Drawing.Size(296, 24)
        Me.Label28.TabIndex = 5
        Me.Label28.Text = "TOTAL:"
        '
        'Label27
        '
        Me.Label27.BackColor = System.Drawing.Color.White
        Me.Label27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label27.Location = New System.Drawing.Point(15, 142)
        Me.Label27.Name = "Label27"
        Me.Label27.Padding = New System.Windows.Forms.Padding(3)
        Me.Label27.Size = New System.Drawing.Size(296, 24)
        Me.Label27.TabIndex = 4
        Me.Label27.Text = "Others:"
        '
        'Label26
        '
        Me.Label26.BackColor = System.Drawing.Color.White
        Me.Label26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label26.Location = New System.Drawing.Point(15, 119)
        Me.Label26.Name = "Label26"
        Me.Label26.Padding = New System.Windows.Forms.Padding(3)
        Me.Label26.Size = New System.Drawing.Size(296, 24)
        Me.Label26.TabIndex = 3
        Me.Label26.Text = "Pick up Fee:"
        '
        'Label25
        '
        Me.Label25.BackColor = System.Drawing.Color.White
        Me.Label25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label25.Location = New System.Drawing.Point(15, 96)
        Me.Label25.Name = "Label25"
        Me.Label25.Padding = New System.Windows.Forms.Padding(3)
        Me.Label25.Size = New System.Drawing.Size(296, 24)
        Me.Label25.TabIndex = 2
        Me.Label25.Text = "Insurance:"
        '
        'Label24
        '
        Me.Label24.BackColor = System.Drawing.Color.White
        Me.Label24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label24.Location = New System.Drawing.Point(15, 73)
        Me.Label24.Name = "Label24"
        Me.Label24.Padding = New System.Windows.Forms.Padding(3)
        Me.Label24.Size = New System.Drawing.Size(296, 24)
        Me.Label24.TabIndex = 1
        Me.Label24.Text = "Valuation:"
        '
        'Label23
        '
        Me.Label23.BackColor = System.Drawing.Color.White
        Me.Label23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label23.Location = New System.Drawing.Point(15, 50)
        Me.Label23.Name = "Label23"
        Me.Label23.Padding = New System.Windows.Forms.Padding(3)
        Me.Label23.Size = New System.Drawing.Size(296, 24)
        Me.Label23.TabIndex = 0
        Me.Label23.Text = "Freight:"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.txtChange)
        Me.GroupBox5.Controls.Add(Me.Label34)
        Me.GroupBox5.Controls.Add(Me.txtTotal)
        Me.GroupBox5.Controls.Add(Me.txtAmountDue)
        Me.GroupBox5.Controls.Add(Me.txtOther)
        Me.GroupBox5.Controls.Add(Me.txtPickupFee)
        Me.GroupBox5.Controls.Add(Me.txtInsurance)
        Me.GroupBox5.Controls.Add(Me.txtVat)
        Me.GroupBox5.Controls.Add(Me.txtValuation)
        Me.GroupBox5.Controls.Add(Me.txtCash)
        Me.GroupBox5.Controls.Add(Me.txtFreight)
        Me.GroupBox5.Controls.Add(Me.Label28)
        Me.GroupBox5.Controls.Add(Me.Label43)
        Me.GroupBox5.Controls.Add(Me.Label27)
        Me.GroupBox5.Controls.Add(Me.Label26)
        Me.GroupBox5.Controls.Add(Me.Label25)
        Me.GroupBox5.Controls.Add(Me.Label40)
        Me.GroupBox5.Controls.Add(Me.Label24)
        Me.GroupBox5.Controls.Add(Me.Label23)
        Me.GroupBox5.Controls.Add(Me.Label29)
        Me.GroupBox5.Location = New System.Drawing.Point(410, 12)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(329, 437)
        Me.GroupBox5.TabIndex = 2
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Payment Details:"
        '
        'txtChange
        '
        Me.txtChange.BackColor = System.Drawing.Color.White
        Me.txtChange.Location = New System.Drawing.Point(206, 255)
        Me.txtChange.Name = "txtChange"
        Me.txtChange.ReadOnly = True
        Me.txtChange.Size = New System.Drawing.Size(100, 20)
        Me.txtChange.TabIndex = 51
        Me.txtChange.TabStop = False
        Me.txtChange.Text = "0.00"
        Me.txtChange.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label34
        '
        Me.Label34.BackColor = System.Drawing.Color.White
        Me.Label34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label34.Location = New System.Drawing.Point(15, 253)
        Me.Label34.Name = "Label34"
        Me.Label34.Padding = New System.Windows.Forms.Padding(3)
        Me.Label34.Size = New System.Drawing.Size(296, 24)
        Me.Label34.TabIndex = 50
        Me.Label34.Text = "CHANGE:"
        Me.Label34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAmountDue
        '
        Me.txtAmountDue.BackColor = System.Drawing.Color.White
        Me.txtAmountDue.Location = New System.Drawing.Point(206, 213)
        Me.txtAmountDue.Name = "txtAmountDue"
        Me.txtAmountDue.ReadOnly = True
        Me.txtAmountDue.Size = New System.Drawing.Size(100, 20)
        Me.txtAmountDue.TabIndex = 49
        Me.txtAmountDue.TabStop = False
        Me.txtAmountDue.Text = "0.00"
        Me.txtAmountDue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtVat
        '
        Me.txtVat.BackColor = System.Drawing.Color.White
        Me.txtVat.Location = New System.Drawing.Point(206, 190)
        Me.txtVat.Name = "txtVat"
        Me.txtVat.ReadOnly = True
        Me.txtVat.Size = New System.Drawing.Size(100, 20)
        Me.txtVat.TabIndex = 46
        Me.txtVat.TabStop = False
        Me.txtVat.Text = "0.00"
        Me.txtVat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCash
        '
        Me.txtCash.BackColor = System.Drawing.Color.White
        Me.txtCash.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCash.Location = New System.Drawing.Point(206, 29)
        Me.txtCash.Name = "txtCash"
        Me.txtCash.Size = New System.Drawing.Size(100, 21)
        Me.txtCash.TabIndex = 35
        Me.txtCash.Text = "0.00"
        Me.txtCash.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label43
        '
        Me.Label43.BackColor = System.Drawing.Color.White
        Me.Label43.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label43.Location = New System.Drawing.Point(15, 211)
        Me.Label43.Name = "Label43"
        Me.Label43.Padding = New System.Windows.Forms.Padding(3)
        Me.Label43.Size = New System.Drawing.Size(296, 24)
        Me.Label43.TabIndex = 34
        Me.Label43.Text = "AMOUNT DUE:"
        Me.Label43.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label40
        '
        Me.Label40.BackColor = System.Drawing.Color.White
        Me.Label40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label40.Location = New System.Drawing.Point(15, 188)
        Me.Label40.Name = "Label40"
        Me.Label40.Padding = New System.Windows.Forms.Padding(3)
        Me.Label40.Size = New System.Drawing.Size(296, 24)
        Me.Label40.TabIndex = 31
        Me.Label40.Text = "12% VAT:"
        Me.Label40.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label29
        '
        Me.Label29.BackColor = System.Drawing.Color.White
        Me.Label29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(15, 26)
        Me.Label29.Name = "Label29"
        Me.Label29.Padding = New System.Windows.Forms.Padding(3)
        Me.Label29.Size = New System.Drawing.Size(296, 25)
        Me.Label29.TabIndex = 20
        Me.Label29.Text = "Cash:"
        Me.Label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(664, 455)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 23)
        Me.btnSave.TabIndex = 3
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(583, 455)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'frmNewShipment
        '
        Me.AcceptButton = Me.btnSave
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(748, 492)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.TabControl1)
        Me.HelpButton = True
        Me.MinimizeBox = False
        Me.Name = "frmNewShipment"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Mailbox and Package Management System"
        Me.TabControl1.ResumeLayout(False)
        Me.tab1.ResumeLayout(False)
        Me.tab1.PerformLayout()
        Me.tab2.ResumeLayout(False)
        Me.tab2.PerformLayout()
        Me.tab3.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.groupPackage.ResumeLayout(False)
        Me.groupPackage.PerformLayout()
        Me.serviceGroup.ResumeLayout(False)
        Me.serviceGroup.PerformLayout()
        Me.tab4.ResumeLayout(False)
        Me.tab4.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tab2 As System.Windows.Forms.TabPage
    Friend WithEvents txtToName As System.Windows.Forms.TextBox
    Friend WithEvents txtToName2 As System.Windows.Forms.TextBox
    Friend WithEvents txtToAddress As System.Windows.Forms.TextBox
    Friend WithEvents txtToCity As System.Windows.Forms.TextBox
    Friend WithEvents txtToCountry As System.Windows.Forms.TextBox
    Friend WithEvents txtToRegion As System.Windows.Forms.TextBox
    Friend WithEvents txtToZip As System.Windows.Forms.TextBox
    Friend WithEvents tab1 As System.Windows.Forms.TabPage
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents tab3 As System.Windows.Forms.TabPage
    Friend WithEvents tab4 As System.Windows.Forms.TabPage
    Friend WithEvents txtFromAddress As System.Windows.Forms.TextBox
    Friend WithEvents txtFromCity As System.Windows.Forms.TextBox
    Friend WithEvents txtFromCountry As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtFromName As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtFromRegion As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtFromZip As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents groupPackage As System.Windows.Forms.GroupBox
    Friend WithEvents serviceGroup As System.Windows.Forms.GroupBox
    Friend WithEvents rdoOrdinaryType As System.Windows.Forms.RadioButton
    Friend WithEvents rdoExpressType As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtWeight As System.Windows.Forms.TextBox
    Friend WithEvents txtPieces As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents txtTotal As System.Windows.Forms.TextBox
    Friend WithEvents txtOther As System.Windows.Forms.TextBox
    Friend WithEvents txtPickupFee As System.Windows.Forms.TextBox
    Friend WithEvents txtInsurance As System.Windows.Forms.TextBox
    Friend WithEvents txtValuation As System.Windows.Forms.TextBox
    Friend WithEvents txtFreight As System.Windows.Forms.TextBox
    Friend WithEvents txtAmountDue As System.Windows.Forms.TextBox
    Friend WithEvents txtVat As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents txtFromMobile As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtFromPhone As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtToMobile As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtToPhone As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtFromFax As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtToFax As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtFromTIN As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtToTIN As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents txtConsignmentNote As System.Windows.Forms.TextBox
    Friend WithEvents txtCash As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents rdoFivePounder As System.Windows.Forms.RadioButton
    Friend WithEvents rdoThreePounder As System.Windows.Forms.RadioButton
    Friend WithEvents rdoOnePounder As System.Windows.Forms.RadioButton
    Friend WithEvents rdoExpressLetter As System.Windows.Forms.RadioButton
    Friend WithEvents rdoEnvelop As System.Windows.Forms.RadioButton
    Friend WithEvents rdoLegalLetter As System.Windows.Forms.RadioButton
    Friend WithEvents rdoBoxLarge As System.Windows.Forms.RadioButton
    Friend WithEvents rdoBoxMedium As System.Windows.Forms.RadioButton
    Friend WithEvents rdoBoxSmall As System.Windows.Forms.RadioButton
    Friend WithEvents rdoGeneralCargo As System.Windows.Forms.RadioButton
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents cboOrigin As System.Windows.Forms.ComboBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents cboDestination As System.Windows.Forms.ComboBox
    Friend WithEvents chInsurance As System.Windows.Forms.CheckBox
    Friend WithEvents chValuation As System.Windows.Forms.CheckBox
    Friend WithEvents txtChange As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
End Class
