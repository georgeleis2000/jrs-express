﻿Imports MySql.Data.MySqlClient

Public Class frmNewShipment

    Dim price_per_grams As Double = 0.0 'Standard package (per type) prices per grams
    Dim express_price As Double = 3.0 'Additional Price for express cargo

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub rdoExpressType_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdoExpressType.CheckedChanged
        If rdoExpressType.Checked = True Then
            rdoBoxLarge.Enabled = True
            rdoBoxMedium.Enabled = True
            rdoBoxSmall.Enabled = True
            rdoExpressLetter.Enabled = True
            rdoOnePounder.Enabled = True
            rdoThreePounder.Enabled = True
            rdoFivePounder.Enabled = True
        Else
            rdoBoxLarge.Enabled = False
            rdoBoxMedium.Enabled = False
            rdoBoxSmall.Enabled = False
            rdoExpressLetter.Enabled = False
            rdoOnePounder.Enabled = False
            rdoThreePounder.Enabled = False
            rdoFivePounder.Enabled = False

            rdoBoxLarge.Checked = False
            rdoBoxMedium.Checked = False
            rdoBoxSmall.Checked = False
            rdoExpressLetter.Checked = False
            rdoOnePounder.Checked = False
            rdoThreePounder.Checked = False
            rdoFivePounder.Checked = False
        End If

        CalculatePayment()
    End Sub

    Private Sub txtFromName_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtFromName.KeyPress, txtFromCountry.KeyPress, txtFromCity.KeyPress
        Dim KeyAscii As Short = Asc(e.KeyChar)

        Select Case KeyAscii
            Case System.Windows.Forms.Keys.Back  '<--- this is for  backspace
            Case 13
                e.Handled = True
                SendKeys.Send("{TAB}")   '<---- use to tab to next textbox or control
                KeyAscii = 0
            Case Is <= 32       'This is for space
                Exit Sub
            Case Is = 46        'Allow period
                Exit Sub
            Case 48 To 57       'this is for numbers
                e.Handled = True
            Case 65 To 90       'this is for Uppercase Alpha 
                Exit Sub
            Case 97 To 122      'this is for Lowercase Alpha 
                Exit Sub
            Case Else
                e.Handled = True
        End Select
    End Sub

    Private Sub txtToName_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtToName2.KeyPress, txtToName.KeyPress, txtToCountry.KeyPress, txtToCity.KeyPress
        Dim KeyAscii As Short = Asc(e.KeyChar)

        Select Case KeyAscii
            Case System.Windows.Forms.Keys.Back  '<--- this is for  backspace
            Case 13
                e.Handled = True
                SendKeys.Send("{TAB}")   '<---- use to tab to next textbox or control
                KeyAscii = 0
            Case Is <= 32       'This is for space
                Exit Sub
            Case Is = 46        'Allow period
                Exit Sub
            Case 48 To 57       'this is for numbers
                e.Handled = True
            Case 65 To 90       'this is for Uppercase Alpha 
                Exit Sub
            Case 97 To 122      'this is for Lowercase Alpha 
                Exit Sub
            Case Else
                e.Handled = True
        End Select
    End Sub

    Private Sub txtFromZip_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtFromZip.KeyPress
        Dim KeyAscii As Short = Asc(e.KeyChar)

        Select Case KeyAscii
            Case System.Windows.Forms.Keys.Back 'Allow backspace
            Case 13
                e.Handled = True
                SendKeys.Send("{TAB}")          'Use to tab to next textbox or control
                KeyAscii = 0
            Case Is <= 32                       'Disallow spaces
                e.Handled = True
            Case Is = 46                        'Disallow period
                e.Handled = True
            Case 48 To 57                       'Allo numbers only
                Exit Sub
            Case 65 To 90                       'Disallow Uppercase Alpha 
                e.Handled = True
            Case 97 To 122                      'Disallow Lowercase Alpha 
                e.Handled = True
            Case Else
                e.Handled = True
        End Select
    End Sub

    Private Sub txtToZip_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtToZip.KeyPress
        Dim KeyAscii As Short = Asc(e.KeyChar)

        Select Case KeyAscii
            Case System.Windows.Forms.Keys.Back 'Allow backspace
            Case 13
                e.Handled = True
                SendKeys.Send("{TAB}")          'Use to tab to next textbox or control
                KeyAscii = 0
            Case Is <= 32                       'Disallow spaces
                e.Handled = True
            Case Is = 46                        'Disallow period
                e.Handled = True
            Case 48 To 57                       'Allo numbers only
                Exit Sub
            Case 65 To 90                       'Disallow Uppercase Alpha 
                e.Handled = True
            Case 97 To 122                      'Disallow Lowercase Alpha 
                e.Handled = True
            Case Else
                e.Handled = True
        End Select
    End Sub

    Private Sub txtCash_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCash.KeyPress
        Dim KeyAscii As Short = Asc(e.KeyChar)

        Select Case KeyAscii
            Case System.Windows.Forms.Keys.Back 'Allow backspace
            Case 13
                e.Handled = True
                SendKeys.Send("{TAB}")          'Use to tab to next textbox or control
                KeyAscii = 0
            Case Is <= 32                       'Disallow spaces
                e.Handled = True
            Case Is = 46                        'Disallow period
                e.Handled = True
            Case 48 To 57                       'Allo numbers only
                Exit Sub
            Case 65 To 90                       'Disallow Uppercase Alpha 
                e.Handled = True
            Case 97 To 122                      'Disallow Lowercase Alpha 
                e.Handled = True
            Case Else
                e.Handled = True
        End Select

        CalculatePayment()
    End Sub

    Private Sub frmNewShipment_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        setFocusMDIChild(Me)
    End Sub

    Private Sub frmNewShipment_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtFromName.Focus()
        frmMain.unloadDashboard(Me)
    End Sub

    Private Function getAllControls(ByVal mainControl As Control, ByVal allControls As List(Of Control)) As List(Of Control)

        If (Not allControls.Contains(mainControl)) Then allControls.Add(mainControl)
        If mainControl.HasChildren Then
            For Each child In mainControl.Controls
                If (Not allControls.Contains(DirectCast(child, Control))) Then allControls.Add(DirectCast(child, Control))
                If DirectCast(child, Control).HasChildren Then getAllControls(DirectCast(child, Control), allControls)
            Next
        End If

        Return allControls

    End Function

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim ctl As Control

        Dim allControls As List(Of Control) = New List(Of Control)
        For Each ctr As Control In Me.Controls
            allControls = getAllControls(ctr, allControls)
        Next

        'Make sure that our controls are not empty
        For Each ctl In allControls
            If TypeOf ctl Is TextBox Or TypeOf ctl Is MaskedTextBox Then
                If ctl.Text = "" Then
                    MsgBox("Please make sure that all fields are properly filled up.", MsgBoxStyle.Information, "JRS Express")
                    ctl.Focus()
                    Return
                End If
            End If
        Next ctl

        Dim cust_id As Integer = insertNewCustomer()

        If cust_id = 0 Then
            MsgBox("Error customer not valid!")
        Else
            Dim code As String

            ' Initialize the random-number generator.
            Randomize()

            ' Generate random value between 1 and 9.
            Dim rdm As New Random
            Dim value As Integer = rdm.Next(0, 100000).ToString("00000") 'CInt(Int((9 * Rnd()) + 1))
            code = Date.Now.ToString("MMdd-") & value & Date.Now.ToString("-yyyy")

            Dim transact_id As Integer = insertNewTransaction(cust_id, code)
            If transact_id = 0 Then
                MsgBox("Error transaction not valid!")
            Else
                'Ship to
                update_transaction_meta(transact_id, "reciever", txtToName.Text)
                update_transaction_meta(transact_id, "reciever2", txtToName2.Text)
                update_transaction_meta(transact_id, "reciever_address", txtToAddress.Text)
                update_transaction_meta(transact_id, "reciever_city", txtToCity.Text)
                update_transaction_meta(transact_id, "reciever_zip", txtToZip.Text)
                update_transaction_meta(transact_id, "reciever_country", txtToCountry.Text)
                update_transaction_meta(transact_id, "reciever_region", txtToRegion.Text)
                update_transaction_meta(transact_id, "reciever_phone", txtToPhone.Text)
                update_transaction_meta(transact_id, "reciever_mobile", txtToMobile.Text)
                update_transaction_meta(transact_id, "reciever_fax", txtToFax.Text)
                update_transaction_meta(transact_id, "reciever_tin", txtToTIN.Text)

                'Shipment Options
                update_transaction_meta(transact_id, "origin", cboOrigin.Text)
                update_transaction_meta(transact_id, "destination", cboDestination.Text)

                If chInsurance.Checked = True Then
                    update_transaction_meta(transact_id, "insurance", "on")
                End If

                If chValuation.Checked = True Then
                    update_transaction_meta(transact_id, "valuation", "on")
                End If

                'Get the package type
                Dim packageType As String = ""
                For Each b As RadioButton In groupPackage.Controls.OfType(Of RadioButton)()
                    If b.Checked = True Then
                        'Get the value and get it's price value
                        packageType = b.Text
                    End If
                Next

                update_transaction_meta(transact_id, "service", packageType)

                'Payment Details
                update_transaction_meta(transact_id, "freight", txtFreight.Text)
                update_transaction_meta(transact_id, "valuation_amount", txtValuation.Text)
                update_transaction_meta(transact_id, "insurance_amount", txtInsurance.Text)
                update_transaction_meta(transact_id, "pickup_fee", txtPickupFee.Text)
                update_transaction_meta(transact_id, "other", txtOther.Text)
                update_transaction_meta(transact_id, "total", txtTotal.Text)
                update_transaction_meta(transact_id, "vat_amount", txtVat.Text)
                update_transaction_meta(transact_id, "change", txtChange.Text)

                ' Clear all the TextBoxes on the form.
                For Each ctl In allControls
                    If TypeOf ctl Is TextBox Then ctl.Text = ""
                    If TypeOf ctl Is MaskedTextBox Then ctl.Text = ""
                    If TypeOf ctl Is ComboBox Then ctl.Text = ""
                Next ctl

                tab1.Focus()
                txtFromName.Focus()

                MsgBox("New Shipment Recorded", MsgBoxStyle.Information, "JRS Express")

                'frmReportsViewer.txtTrans_ID.Text = code
                frmReportsViewer.RecieptDataTableAdapter.Fill(frmReportsViewer.gdl_jrs_devDataSet.RecieptData, code)
                frmReportsViewer.MdiParent = frmMain
                frmReportsViewer.Show()

            End If
        End If

    End Sub

    Private Sub update_transaction_meta(ByVal trans_id As Integer, ByVal meta_key As String, ByVal value As String)
        Dim con = NewConnection()
        Dim Result As String
        Dim cmd As New MySqlCommand

        With cmd
            .Connection = con
            .CommandText = "INSERT INTO `jrs_transaction_meta` (`trans_id`, `meta_key`, `value`) " & _
                            "VALUES ('" & trans_id & "', '" & meta_key & "', '" & value & "');"

            'in this line it Executes a transact-SQL statements against the connection and returns the number of rows affected 
            Result = cmd.ExecuteNonQuery

            'if the result is equal to zero it means that no rows is inserted or somethings wrong during the execution
            If Result = 0 Then
                MsgBox("Error: Adding " & meta_key & " - transaction meta!")
            End If
        End With
    End Sub

    Private Function insertNewTransaction(ByVal cust_id As Integer, ByVal code As String)
        Dim con = NewConnection()
        Dim Result As String
        Dim cmd As New MySqlCommand
        Dim user As String
        Dim serviceType As String = ""
        Dim datenow As String = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")
        Dim shipment_schedule As String

        'Get the package type
        For Each b As RadioButton In serviceGroup.Controls.OfType(Of RadioButton)()
            If b.Checked = True Then
                'Get the value and get it's price value
                serviceType = b.Text
            End If
        Next

        If serviceType <> "EXPRESS" Then
            shipment_schedule = DateTime.Now.AddDays(+7).ToString("yyyy/MM/dd HH:mm:ss")
        Else
            shipment_schedule = DateTime.Now.AddDays(+1).ToString("yyyy/MM/dd HH:mm:ss")
        End If

        If logged_in_user = 0 Then
            user = "admin"
        Else
            user = get_user_by_id(logged_in_user, "display_name")
        End If

        With cmd
            .Connection = con
            .CommandText = "INSERT INTO `jrs_transactions` (`code`, `user_id`, `customer_id`, `amount`, `payment_amount`, `pieces`, `weight`, `type`, `date`, `note`, `shipment_status`, `shipment_schedule`, `status`) " & _
                "VALUES ('" & code & "', '" & logged_in_user & "', '" & cust_id & "', '" & txtAmountDue.Text & "', " & _
                "'" & txtCash.Text & "', '" & txtPieces.Text & "', " & _
                "'" & txtWeight.Text & "', '" & serviceType & "', '" & datenow & "', " & _
                "'" & txtConsignmentNote.Text & "', 'for shipping', '" & shipment_schedule & "', 'paid');"

            'in this line it Executes a transact-SQL statements against the connection and returns the number of rows affected 
            Result = cmd.ExecuteNonQuery

            'if the result is equal to zero it means that no rows is inserted or something is wrong during the execution
            If Result = 0 Then
                MsgBox("Error adding new customer!")
                Return 0
            Else
                If cmd.LastInsertedId = vbNull Then
                    cmd.Parameters.Add(New MySqlParameter("@ID", cmd.LastInsertedId))
                End If

                Return cmd.LastInsertedId 'Convert.ToInt32(cmd.Parameters("@ID").Value)
            End If
        End With

        Return 0
    End Function

    Private Function insertNewCustomer()
        Dim con = NewConnection()
        Dim Result As String
        Dim cmd As New MySqlCommand

        With cmd
            .Connection = con
            .CommandText = "INSERT INTO `jrs_customers` (`nicename`, `address`, `city`, `zip`, `country`, " & _
                "`region`, `phone`, `mobile`, `fax`, `tin`, `status`) " & _
                "VALUES ('" & txtFromName.Text & "', '" & txtFromAddress.Text & "', '" & txtFromCity.Text & _
                "', '" & txtFromZip.Text & "', '" & txtFromCountry.Text & "', '" & txtFromRegion.Text & _
                "', '" & txtFromPhone.Text & "', '" & txtFromMobile.Text & "', '" & txtFromFax.Text & _
                "', '" & txtFromTIN.Text & "', 'active');"

            'in this line it Executes a transact-SQL statements against the connection and returns the number of rows affected 
            Result = cmd.ExecuteNonQuery

            'if the result is equal to zero it means that no rows is inserted or something is wrong during the execution
            If Result = 0 Then
                MsgBox("Error adding new customer!")
                Return 0
            Else
                If cmd.LastInsertedId = vbNull Then
                    cmd.Parameters.Add(New MySqlParameter("@ID", cmd.LastInsertedId))
                End If

                Return cmd.LastInsertedId 'Convert.ToInt32(cmd.Parameters("@ID").Value)
            End If
        End With

        Return 0
    End Function

    Private Sub txtPieces_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtWeight.KeyPress, txtPieces.KeyPress
        Dim KeyAscii As Short = Asc(e.KeyChar)

        Select Case KeyAscii
            Case System.Windows.Forms.Keys.Back 'Allow backspace
            Case 13
                e.Handled = True
                SendKeys.Send("{TAB}")          'Use to tab to next textbox or control
                KeyAscii = 0
            Case Is <= 32                       'Disallow spaces
                e.Handled = True
            Case Is = 46                        'Disallow period
                Exit Sub
            Case 48 To 57                       'Allo numbers only
                Exit Sub
            Case 65 To 90                       'Disallow Uppercase Alpha 
                e.Handled = True
            Case 97 To 122                      'Disallow Lowercase Alpha 
                e.Handled = True
            Case Else
                e.Handled = True
        End Select

        CalculatePayment()
    End Sub

    Private Sub txtPieces_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtWeight.TextChanged, txtPieces.TextChanged
        CalculatePayment()
    End Sub

    Private Sub rdoLegalLetter_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdoThreePounder.CheckedChanged, rdoOnePounder.CheckedChanged, rdoLegalLetter.CheckedChanged, rdoGeneralCargo.CheckedChanged, rdoFivePounder.CheckedChanged, rdoExpressLetter.CheckedChanged, rdoEnvelop.CheckedChanged, rdoBoxSmall.CheckedChanged, rdoBoxMedium.CheckedChanged, rdoBoxLarge.CheckedChanged
        CalculatePayment()
    End Sub

    Private Sub CalculatePayment()
        'Package Price list
        Dim packageType As String = ""

        'Get the number of pieces
        Dim pieces As String = txtPieces.Text

        'Get the number of weight per pieces
        Dim weight As String = txtWeight.Text

        Dim Freight As Double

        'Get the package type
        For Each b As RadioButton In groupPackage.Controls.OfType(Of RadioButton)()
            If b.Checked = True Then
                'Get the value and get it's price value
                packageType = b.Text
            End If
        Next

        'Set the price_per_grams according to the selected package
        Select Case packageType
            Case "Legal Size Letter"
                price_per_grams = 10.0
            Case "Brown Envelop Letter"
                price_per_grams = 11.0
            Case "Express Legal Letter"
                price_per_grams = 20.0
            Case "One Pounder"
                price_per_grams = 22.0
            Case "Three Pounder"
                price_per_grams = 24.0
            Case "Five Pounder"
                price_per_grams = 25.0
            Case "General Cargo"
                price_per_grams = 30.0
            Case "Express Box Small"
                price_per_grams = 33.0
            Case "Express Box Medium"
                price_per_grams = 35.0
            Case "Express Box Large"
                price_per_grams = 40.0
        End Select

        Freight = Val(Val(price_per_grams) * Val(weight)) * Val(pieces)
        txtFreight.Text = Format(Val(Freight), ".00")

        If chInsurance.Checked = True Then
            txtInsurance.Text = Format(Val(Val(Freight) * Val(".02")), ".00")
        End If

        If chValuation.Checked = True Then
            txtValuation.Text = Format(Val(Val(Freight) * Val(".02")), ".00")
        End If

        txtTotal.Text = Format(Val(Val(Freight + Val(txtValuation.Text) + Val(txtInsurance.Text) + Val(txtPickupFee.Text) + Val(txtOther.Text))), ".00")
        txtVat.Text = Format(Val(txtTotal.Text * Val(".12")), ".00")

        txtAmountDue.Text = Format(Val(txtTotal.Text), ".00")

        If Val(txtAmountDue.Text) < Val(txtCash.Text) Then
            txtChange.Text = Format(Val(Val(txtCash.Text) - Val(txtAmountDue.Text)), ".00")
        End If
    End Sub

    Private Sub chInsurance_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chValuation.CheckedChanged, chInsurance.CheckedChanged
        CalculatePayment()
    End Sub

    Private Sub txtCash_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCash.TextChanged
        txtCash.Text = Format(Val(txtCash.Text), ".00")
        CalculatePayment()
    End Sub

    Private Sub txtFromCity_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFromRegion.TextChanged, txtFromCity.TextChanged
        Dim resultIndex As Integer = -1

        resultIndex = cboOrigin.FindStringExact(txtFromCity.Text)
        If resultIndex > -1 Then
            cboOrigin.SelectedIndex = resultIndex
        Else
            resultIndex = cboOrigin.FindStringExact(txtFromRegion.Text)
            If resultIndex > -1 Then
                cboOrigin.SelectedIndex = resultIndex
            End If
        End If
    End Sub

    Private Sub txtToCity_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtToRegion.TextChanged, txtToCity.TextChanged
        Dim resultIndex As Integer = -1

        resultIndex = cboDestination.FindStringExact(txtToCity.Text)
        If resultIndex > -1 Then
            cboDestination.SelectedIndex = resultIndex
        Else
            resultIndex = cboDestination.FindStringExact(txtToRegion.Text)
            If resultIndex > -1 Then
                cboDestination.SelectedIndex = resultIndex
            End If
        End If
    End Sub

End Class