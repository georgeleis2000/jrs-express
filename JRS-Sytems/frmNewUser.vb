﻿Imports MySql.Data.MySqlClient

Public Class frmNewUser

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Dim ctl As Control

        ' Clear all the TextBoxes on the form.
        For Each ctl In Controls
            If TypeOf ctl Is TextBox Then ctl.Text = ""
        Next ctl
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim ctl As Control

        'Make sure that our controls are not empty
        For Each ctl In Controls
            If TypeOf ctl Is TextBox Then
                If ctl.Text = "" Then
                    MsgBox("All fields are required!")
                    ctl.Focus()
                    Return
                End If
            End If
        Next ctl

        Dim con = NewConnection()
        Dim Result As String
        Dim cmd As New MySqlCommand

        Dim newPassword As String = getMD5Hash(txtPassword.Text)

        With cmd
            .Connection = con
            .CommandText = "INSERT INTO `jrs_users` (`user_login`, `user_pass`, `display_name`, `user_status`) " & _
                            "VALUES ('" & txtUserLogin.Text & "', '" & newPassword & "', '" & txtName.Text & "', '" & cboStatus.Text & "' );"

            'in this line it Executes a transact-SQL statements against the connection and returns the number of rows affected 
            Result = cmd.ExecuteNonQuery

            'if the result is equal to zero it means that no rows is inserted or somethings wrong during the execution
            If Result = 0 Then
                MsgBox("Error adding new user!")
            Else
                ' Clear all the TextBoxes on the form.
                For Each ctl In Controls
                    If TypeOf ctl Is TextBox Then ctl.Text = ""
                Next ctl
                txtName.Focus()

                MsgBox("New user added successfully!")
            End If
        End With

        con.Close()
    End Sub

    Private Sub chShowPassword_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chShowPassword.CheckedChanged
        If chShowPassword.Checked <> True Then
            txtPassword.PasswordChar = "*"
        Else
            txtPassword.PasswordChar = ""
        End If
    End Sub

    Private Sub txtName_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtName.KeyPress
        Dim KeyAscii As Short = Asc(e.KeyChar)

        Select Case KeyAscii
            Case System.Windows.Forms.Keys.Back  '<--- this is for  backspace
            Case 13
                e.Handled = True
                SendKeys.Send("{TAB}")   '<---- use to tab to next textbox or control
                KeyAscii = 0
            Case Is <= 32       'This is for space
                Exit Sub
            Case Is = 46        'Allow period
                Exit Sub
            Case 48 To 57       'this is for numbers
                e.Handled = True
            Case 65 To 90       'this is for Uppercase Alpha 
                Exit Sub
            Case 97 To 122      'this is for Lowercase Alpha 
                Exit Sub
            Case Else
                e.Handled = True
        End Select
    End Sub

    Private Sub txtUserLogin_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtUserLogin.KeyPress
        Dim KeyAscii As Short = Asc(e.KeyChar)

        Select Case KeyAscii
            Case System.Windows.Forms.Keys.Back  '<--- this is for  backspace
            Case 13
                e.Handled = True
                SendKeys.Send("{TAB}")   '<---- use to tab to next textbox or control
                KeyAscii = 0
            Case Is <= 32       'Disallow space
                e.Handled = True
            Case Is = 46        'Allow period
                Exit Sub
            Case 48 To 57       'Allow numbers
                Exit Sub
            Case 65 To 90       'Allow Uppercase Alpha 
                Exit Sub
            Case Is = 95        'Allow underscores
                Exit Sub
            Case 97 To 122      'Allow Lowercase Alpha 
                Exit Sub
            Case Else
                e.Handled = True
        End Select
    End Sub

    Private Sub txtName_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtUserLogin.Enter, txtPassword.Enter, txtName.Enter
        Dim txtbox As TextBox
        If TypeOf sender Is TextBox Then
            txtbox = DirectCast(sender, TextBox)

            If (Not String.IsNullOrEmpty(txtbox.Text)) Then
                txtbox.SelectionStart = 0
                txtbox.SelectionLength = Len(txtName.Text)
            End If
        End If
    End Sub

    Private Sub frmNewUser_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        setFocusMDIChild(Me)
    End Sub

    Private Sub frmNewUser_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        frmMain.unloadDashboard(Me)
    End Sub
End Class