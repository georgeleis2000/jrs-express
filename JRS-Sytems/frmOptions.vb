﻿Imports MySql.Data.MySqlClient

Public Class frmOptions

    Dim con As New MySqlConnection
    Dim result As Integer

    'MySqlCommand It represents a SQL statement to execute against a MySQL Database
    Dim cmd As New MySqlCommand

    'Next will add code to our btnSave button and here’s the code.

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'this line of code is simply we copy this one from our form
        con.ConnectionString = ("server=localhost;user id=root;password=;database=gdl_jrs_dev")

        Try
            'we open Connection
            con.Open()

            With cmd
                .Connection = con
                .CommandText = "INSERT INTO `jrs_options` (`key`, `value`) " & _
                                "VALUES ('admin_username', '" & txtUsername.Text & "'), " & _
                                "('admin_password', '" & txtPassword.Text & "'), " & _
                                "('branch_code', '" & txtBranchCode.Text & "'), " & _
                                "('branch_name', '" & txtBranchName.Text & "'), " & _
                                "('branch_address', '" & txtBranchAddress.Text & "'), " & _
                                "('branch_phone', '" & txtPhone.Text & "'), " & _
                                "('branch_fax', '" & txtFax.Text & "') " & _
                                "ON DUPLICATE KEY " & _
                                "UPDATE value=VALUES(value);"

                'in this line it Executes a transact-SQL statements against the connection and returns the number of rows affected 
                result = cmd.ExecuteNonQuery

                'if the result is equal to zero it means that no rows is inserted or somethings wrong during the execution
                If result = 0 Then
                    MsgBox("Data has been Inserted!")
                Else
                    MsgBox("Successfully saved!")
                    'Me.Close()
                End If
            End With
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        con.Close()
    End Sub

    Private Sub chShowPassword_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chShowPassword.CheckedChanged
        If chShowPassword.Checked <> True Then
            txtPassword.PasswordChar = "*"
        Else
            txtPassword.PasswordChar = ""
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub frmOptions_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        setFocusMDIChild(Me)
    End Sub

    Private Sub frmOptions_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim super_user As String = get_option("admin_username")
        Dim super_pass As String = get_option("admin_password")
        Dim brCode As String = get_option("branch_code")
        Dim brName As String = get_option("branch_name")
        Dim brAddress As String = get_option("branch_address")
        Dim brPhone As String = get_option("branch_phone")
        Dim brFax As String = get_option("branch_fax")

        txtUsername.Text = super_user
        txtPassword.Text = super_pass
        txtBranchCode.Text = brCode
        txtBranchName.Text = brName
        txtBranchAddress.Text = brAddress
        txtPhone.Text = brPhone
        txtFax.Text = brFax

        frmMain.unloadDashboard(Me)
    End Sub

    Private Sub txtUsername_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtUsername.KeyPress
        Dim KeyAscii As Short = Asc(e.KeyChar)

        Select Case KeyAscii
            Case System.Windows.Forms.Keys.Back  '<--- this is for  backspace
            Case 13
                e.Handled = True
                SendKeys.Send("{TAB}")   '<---- use to tab to next textbox or control
                KeyAscii = 0
            Case Is <= 32       'Disallow space
                e.Handled = True
            Case Is = 46        'Allow period
                Exit Sub
            Case 48 To 57       'Allow numbers
                Exit Sub
            Case 65 To 90       'Allow Uppercase Alpha 
                Exit Sub
            Case Is = 95        'Allow underscores
                Exit Sub
            Case 97 To 122      'Allow Lowercase Alpha 
                Exit Sub
            Case Else
                e.Handled = True
        End Select
    End Sub

    Private Sub txtBranchCode_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBranchCode.KeyPress
        Dim KeyAscii As Short = Asc(e.KeyChar)

        Select Case KeyAscii
            Case System.Windows.Forms.Keys.Back 'Allow backspace
            Case 13
                e.Handled = True
                SendKeys.Send("{TAB}")          'Use to tab to next textbox or control
                KeyAscii = 0
            Case Is <= 32                       'Disallow spaces
                e.Handled = True
            Case Is = 46                        'Disallow period
                e.Handled = True
            Case 48 To 57                       'Allow numbers only
                Exit Sub
            Case 65 To 90                       'Disallow Uppercase Alpha 
                e.Handled = True
            Case 97 To 122                      'Disallow Lowercase Alpha 
                e.Handled = True
            Case Else
                e.Handled = True
        End Select
    End Sub

End Class