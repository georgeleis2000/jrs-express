﻿Imports MySql.Data.MySqlClient

Public Class login

    Dim con As New MySqlConnection
    Dim result As Integer

    'MySqlCommand It represents a SQL statement to execute against a MySQL Database
    Dim cmd As New MySqlCommand

    Private Sub login_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    'Next will add code to our btnSave button and here’s the code.

    Private Sub mainMDI_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtUsername.Focus()
    End Sub

    Private Sub btnLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Dim username As String
        Dim password As String
        Dim user As String

        username = get_option("admin_username")
        password = get_option("admin_password")

        user = get_user_by_login(txtUsername.Text, "")

        'Check if super admin
        If txtUsername.Text = username Then
            If txtPassword.Text = password Then
                Me.Close()
                is_admin = True
                frmMain.MenuStrip.Enabled = True
                frmMain.ToolStrip.Enabled = True
                frmMain.statusLblEmployee.Text = "Admin"
                Me.DialogResult = Windows.Forms.DialogResult.OK
            Else
                MsgBox("Wrong Password", MsgBoxStyle.OkOnly, "JRS Login")
                txtPassword.Text = ""
                txtPassword.Focus()
            End If
        Else
            ' Check if user_login is assigned from an empleyado
            If user <> "" Then
                If user = getMD5Hash(txtPassword.Text) Then
                    Me.Close()
                    is_admin = False
                    frmMain.MenuStrip.Enabled = True
                    frmMain.ToolStrip.Enabled = True
                    logged_in_user = 1
                    frmMain.statusLblEmployee.Text = get_user_by_login(txtUsername.Text, "display_name")
                    Me.DialogResult = Windows.Forms.DialogResult.OK
                Else
                    MsgBox("Wrong Password", MsgBoxStyle.OkOnly, "JRS Login")
                    txtPassword.Text = ""
                    txtPassword.Focus()
                End If
            Else
                MsgBox("Wrong Username", MsgBoxStyle.OkOnly, "JRS Login")
                txtUsername.Text = ""
                txtUsername.Focus()
            End If
        End If

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

End Class