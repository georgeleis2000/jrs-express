﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReportsViewer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource2 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.txtTrans_ID = New System.Windows.Forms.TextBox()
        Me.RecieptDataBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.gdl_jrs_devDataSet = New WindowsApplication1.gdl_jrs_devDataSet()
        Me.RecieptDataTableAdapter = New WindowsApplication1.gdl_jrs_devDataSetTableAdapters.RecieptDataTableAdapter()
        CType(Me.RecieptDataBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gdl_jrs_devDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource2.Name = "DataSet1"
        ReportDataSource2.Value = Me.RecieptDataBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource2)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "WindowsApplication1.rptOfficialReciept.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(500, 389)
        Me.ReportViewer1.TabIndex = 0
        '
        'txtTrans_ID
        '
        Me.txtTrans_ID.Location = New System.Drawing.Point(41, 57)
        Me.txtTrans_ID.Name = "txtTrans_ID"
        Me.txtTrans_ID.Size = New System.Drawing.Size(100, 20)
        Me.txtTrans_ID.TabIndex = 1
        Me.txtTrans_ID.Visible = False
        '
        'RecieptDataBindingSource
        '
        Me.RecieptDataBindingSource.DataMember = "RecieptData"
        Me.RecieptDataBindingSource.DataSource = Me.gdl_jrs_devDataSet
        '
        'gdl_jrs_devDataSet
        '
        Me.gdl_jrs_devDataSet.DataSetName = "gdl_jrs_devDataSet"
        Me.gdl_jrs_devDataSet.EnforceConstraints = False
        Me.gdl_jrs_devDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'RecieptDataTableAdapter
        '
        Me.RecieptDataTableAdapter.ClearBeforeFill = True
        '
        'frmReportsViewer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(500, 389)
        Me.Controls.Add(Me.txtTrans_ID)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "frmReportsViewer"
        Me.Text = "Official Reciept"
        CType(Me.RecieptDataBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gdl_jrs_devDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents RecieptDataBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents gdl_jrs_devDataSet As WindowsApplication1.gdl_jrs_devDataSet
    Friend WithEvents RecieptDataTableAdapter As WindowsApplication1.gdl_jrs_devDataSetTableAdapters.RecieptDataTableAdapter
    Friend WithEvents txtTrans_ID As System.Windows.Forms.TextBox


End Class
