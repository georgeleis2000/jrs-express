﻿Imports MySql.Data.MySqlClient

Public Class frmNewBranch

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Dim ctl As Control

        ' Clear all the TextBoxes on the form.
        For Each ctl In Controls
            If TypeOf ctl Is TextBox Then ctl.Text = ""
        Next ctl
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim ctl As Control

        'Make sure that our controls are not empty
        For Each ctl In Controls
            If TypeOf ctl Is TextBox Or TypeOf ctl Is MaskedTextBox Then
                If ctl.Text = "" Then
                    MsgBox("All fields are required!")
                    ctl.Focus()
                    Return
                End If
            End If
        Next ctl

        Dim con = NewConnection()
        Dim Result As String
        Dim cmd As New MySqlCommand

        With cmd
            .Connection = con
            .CommandText = "INSERT INTO `jrs_branches` (`code`, `name`, `manager`, `address`, `city`, " & _
                            "`state`, `zip`, `country`, `phone`, `fax`) " & _
                            "VALUES ('" & txtCode.Text & "', '" & txtName.Text & "', '" & txtManager.Text & "', '" & _
                            txtAddress.Text & "', '" & txtCity.Text & "', '" & txtState.Text & "', '" & txtZip.Text & _
                            "', '" & txtCountry.Text & "', '" & txtPhone.Text & "', '" & txtFax.Text & "' );"

            'in this line it Executes a transact-SQL statements against the connection and returns the number of rows affected 
            Result = cmd.ExecuteNonQuery

            'if the result is equal to zero it means that no rows is inserted or somethings wrong during the execution
            If Result = 0 Then
                MsgBox("Error adding new branch!")
            Else
                ' Clear all the TextBoxes on the form.
                For Each ctl In Controls
                    If TypeOf ctl Is TextBox Then ctl.Text = ""
                    If TypeOf ctl Is MaskedTextBox Then ctl.Text = ""
                Next ctl
                txtCode.Focus()

                MsgBox("New branch added successfully!")
            End If
        End With
    End Sub

    Private Sub txtManager_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtState.KeyPress, txtManager.KeyPress, txtCountry.KeyPress, txtCity.KeyPress
        Dim KeyAscii As Short = Asc(e.KeyChar)

        Select Case KeyAscii
            Case System.Windows.Forms.Keys.Back  '<--- this is for  backspace
            Case 13
                e.Handled = True
                SendKeys.Send("{TAB}")   '<---- use to tab to next textbox or control
                KeyAscii = 0
            Case Is <= 32       'This is for space
                Exit Sub
            Case Is = 46        'Allow period
                Exit Sub
            Case 48 To 57       'this is for numbers
                e.Handled = True
            Case 65 To 90       'this is for Uppercase Alpha 
                Exit Sub
            Case 97 To 122      'this is for Lowercase Alpha 
                Exit Sub
            Case Else
                e.Handled = True
        End Select
    End Sub

    Private Sub txtZip_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtZip.KeyPress, txtCode.KeyPress
        Dim KeyAscii As Short = Asc(e.KeyChar)

        Select Case KeyAscii
            Case System.Windows.Forms.Keys.Back 'Allow backspace
            Case 13
                e.Handled = True
                SendKeys.Send("{TAB}")          'Use to tab to next textbox or control
                KeyAscii = 0
            Case Is <= 32                       'Disallow spaces
                e.Handled = True
            Case Is = 46                        'Disallow period
                e.Handled = True
            Case 48 To 57                       'Allo numbers only
                Exit Sub
            Case 65 To 90                       'Disallow Uppercase Alpha 
                e.Handled = True
            Case 97 To 122                      'Disallow Lowercase Alpha 
                e.Handled = True
            Case Else
                e.Handled = True
        End Select
    End Sub

    Private Sub frmNewBranch_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        setFocusMDIChild(Me)
    End Sub

    Private Sub frmNewBranch_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        frmMain.unloadDashboard(Me)
    End Sub

End Class