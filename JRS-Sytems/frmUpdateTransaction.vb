﻿Imports MySql.Data.MySqlClient

Public Class frmUpdateTransaction

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub cboStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboStatus.SelectedIndexChanged
        If cboStatus.Text = "Custom" Then
            txtCustomStatus.Visible = True
        Else
            txtCustomStatus.Visible = False
        End If
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim status As String

        If cboStatus.Text = "" Then
            Exit Sub
        ElseIf cboStatus.Text = "Custom" Then
            status = txtCustomStatus.Text
        Else
            status = cboStatus.Text
        End If

        If status = "" Then
            Exit Sub
        Else
            Dim con = NewConnection()
            Dim Result As String
            Dim cmd As New MySqlCommand

            With cmd
                .Connection = con
                .CommandText = "UPDATE `jrs_transactions` SET `shipment_status` = '" & status & "' " & _
                    "WHERE id = " & txtTrans_ID.Text & ";"

                'in this line it Executes a transact-SQL statements against the connection and returns the number of rows affected 
                Result = cmd.ExecuteNonQuery

                'if the result is equal to zero it means that no rows is inserted or somethings wrong during the execution
                If Result = 0 Then
                    MsgBox("Error updating transaction!")
                    Me.DialogResult = DialogResult.Cancel
                Else
                    MsgBox("Transaction Updated successfully!")
                    Me.DialogResult = DialogResult.OK
                End If
            End With
        End If

    End Sub

    Private Sub frmUpdateTransaction_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

End Class