-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 04, 2015 at 01:28 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gdl_jrs_dev`
--

-- --------------------------------------------------------

--
-- Table structure for table `jrs_branches`
--

CREATE TABLE IF NOT EXISTS `jrs_branches` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` bigint(4) NOT NULL,
  `name` varchar(250) NOT NULL,
  `manager` varchar(500) NOT NULL,
  `address` varchar(300) NOT NULL,
  `city` varchar(250) NOT NULL,
  `state` varchar(250) NOT NULL,
  `zip` varchar(250) NOT NULL,
  `country` varchar(250) NOT NULL,
  `phone` varchar(250) NOT NULL,
  `fax` varchar(250) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `jrs_branches`
--

INSERT INTO `jrs_branches` (`ID`, `code`, `name`, `manager`, `address`, `city`, `state`, `zip`, `country`, `phone`, `fax`) VALUES
(1, 321, 'Head Office', 'Marco Mediola', 'No. 19 Brixton Street Brgy. Kapitoloyo', 'Pasig City', 'Metro Manila', '1600', 'Philippines', '(02) 631-73-51', '(02) 631-73-52'),
(2, 322, 'Jollibee Plaza', 'Sherwin Palacpac', 'Ground Floor Jollibee Plaza, F. Ortigas Jr. Road, Ortigas Center', 'Pasig City', 'Metro Manila', '1600', 'Philippines', '( 02) 638-9194', '( 02) 638-9195');

-- --------------------------------------------------------

--
-- Table structure for table `jrs_customermeta`
--

CREATE TABLE IF NOT EXISTS `jrs_customermeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`meta_id`),
  KEY `user_id` (`customer_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=92 ;

-- --------------------------------------------------------

--
-- Table structure for table `jrs_customers`
--

CREATE TABLE IF NOT EXISTS `jrs_customers` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nicename` varchar(50) NOT NULL DEFAULT '',
  `address` varchar(100) NOT NULL DEFAULT '',
  `city` varchar(250) NOT NULL,
  `zip` int(5) NOT NULL,
  `country` varchar(250) NOT NULL,
  `region` varchar(250) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `fax` varchar(50) NOT NULL,
  `tin` varchar(50) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `jrs_customers`
--

INSERT INTO `jrs_customers` (`ID`, `nicename`, `address`, `city`, `zip`, `country`, `region`, `phone`, `mobile`, `fax`, `tin`, `status`) VALUES
(9, 'George Daniel Leis', '# 257 D. San Jose St. BRGY. San Isidro', 'Antipolo City', 1780, 'Philippines', 'Rizal', '(978) 686-7895', '+63 959-565-9559', '(958) 589-5985', '895-858-759-855', 0),
(10, 'Ronald Butlay', '# 67 Blk. 42 Lot 67', 'Cainta', 1780, 'Philippines', 'Rizal', '(897) 896-8698', '+63 595-858-5985', '(585) 859-8957', '589-585-895-858', 0),
(11, 'Joyce Ann R. Reyes', '# 53 G. Bunyi St. Buting', 'Pasig City', 1600, 'Philippines', 'Metro Manila', '(675) 756-8557', '+63 876-867-8675', '(856) 785-6785', '756-785-875-856', 0),
(12, 'paolo redelicia', 'tAGUIG CITY', 'TAGUIG', 1630, 'Philippines', 'NCR', '(221) 321-3123', '+63 123-123-1231', '(123) 123-123', '23 -231-325-325', 0),
(13, 'vanessa', 'taguig city', 'ghff', 56567, 'Philippines', 'ghcbvch', '(091) 920-424', '+63    - 45-4646', '(   )    -   7', ' 76-465-456-456', 0),
(14, 'zdSddsd', 'sdsdsddsdsd', 'dsds', 45555, 'Philippines', 'zfrsdf', '(   )    - 343', '+63    -   -', '(   )    -', '   -   -   -', 0),
(15, 'Joshua', '401 2nd guada', 'makati', 1200, 'Philippines', 'jasdjasd', '( 09) 090-9090', '+63 892-980-9898', '(128) 989-8989', '898-989-898-98', 0);

-- --------------------------------------------------------

--
-- Table structure for table `jrs_freight`
--

CREATE TABLE IF NOT EXISTS `jrs_freight` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ship_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ship_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `contents` longtext NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'publish',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `type` varchar(20) NOT NULL DEFAULT 'post',
  `commodity_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1405 ;

--
-- Dumping data for table `jrs_freight`
--

INSERT INTO `jrs_freight` (`ID`, `ship_date`, `ship_date_gmt`, `contents`, `status`, `date_modified`, `date_modified_gmt`, `type`, `commodity_count`) VALUES
(1404, '2014-09-30 00:00:00', '2014-09-30 00:00:00', '', 'New', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'freight', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jrs_freight_meta`
--

CREATE TABLE IF NOT EXISTS `jrs_freight_meta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `freight_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`freight_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6214 ;

-- --------------------------------------------------------

--
-- Table structure for table `jrs_options`
--

CREATE TABLE IF NOT EXISTS `jrs_options` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `key` varchar(100) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `jrs_options`
--

INSERT INTO `jrs_options` (`ID`, `key`, `value`) VALUES
(2, 'admin_username', 'admin'),
(3, 'admin_password', 'password'),
(4, 'branch_code', '321'),
(5, 'branch_name', 'Head Office'),
(6, 'branch_address', 'No. 19 Brixton Street Brgy. Kapitolyo Pasig City Metro Manila'),
(7, 'branch_phone', '(002) 631-7351'),
(8, 'branch_fax', '(002) 631-7352');

-- --------------------------------------------------------

--
-- Table structure for table `jrs_transactions`
--

CREATE TABLE IF NOT EXISTS `jrs_transactions` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(250) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `customer_id` bigint(20) NOT NULL,
  `amount` varchar(50) NOT NULL,
  `payment_amount` varchar(50) NOT NULL,
  `pieces` int(20) NOT NULL,
  `weight` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `date` datetime NOT NULL,
  `note` longtext NOT NULL,
  `shipment_status` varchar(250) NOT NULL,
  `shipment_schedule` datetime NOT NULL,
  `status` varchar(250) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `jrs_transactions`
--

INSERT INTO `jrs_transactions` (`ID`, `code`, `user_id`, `customer_id`, `amount`, `payment_amount`, `pieces`, `weight`, `type`, `date`, `note`, `shipment_status`, `shipment_schedule`, `status`) VALUES
(5, '1011-9478-2014', 1, 9, '1560.00', '2000.00', 3, '25', 'EXPRESS', '2014-10-11 03:31:51', 'Lorem ipsum', 'Damaged', '2014-10-12 03:31:51', 'paid'),
(6, '1011-24127-2014', 1, 10, '686.40', '1000.00', 4, '15', 'ORDINARY', '2014-10-11 03:43:13', 'Some note', 'Returned', '2014-10-18 03:43:13', 'paid'),
(7, '1011-38839-2014', 0, 11, '4800.00', '5000.00', 2, '80', 'ORDINARY', '2014-10-11 07:55:57', 'Note kahit ano', 'for shipping', '2014-10-18 07:55:57', 'paid'),
(8, '1011-71772-2014', 0, 12, '29835.00', '2321231.00', 3, '325', 'ORDINARY', '2014-10-11 10:00:16', 'pogi si paolo', 'for shipping', '2014-10-18 10:00:16', 'paid'),
(9, '1011-64183-2014', 0, 13, '33000.00', '50000.00', 10, '100', 'EXPRESS', '2014-10-11 10:22:49', 'good morning ! ', 'for shipping', '2014-10-12 10:22:49', 'paid'),
(10, '1011-55017-2014', 0, 14, '14320.80', '.00', 2, '234', 'ORDINARY', '2014-10-11 13:38:33', 'waryfgatjuhn', 'for shipping', '2014-10-18 13:38:33', 'paid'),
(11, '1011-67024-2014', 0, 15, '275400.00', '100000.00', 100, '90', 'EXPRESS', '2014-10-11 14:00:49', 'hahahahaha', 'for shipping', '2014-10-12 14:00:49', 'paid');

-- --------------------------------------------------------

--
-- Table structure for table `jrs_transaction_meta`
--

CREATE TABLE IF NOT EXISTS `jrs_transaction_meta` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `trans_id` bigint(20) NOT NULL,
  `meta_key` varchar(250) NOT NULL,
  `value` longtext NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=234 ;

--
-- Dumping data for table `jrs_transaction_meta`
--

INSERT INTO `jrs_transaction_meta` (`ID`, `trans_id`, `meta_key`, `value`) VALUES
(73, 5, 'reciever', 'Joyce Ann R. Reyes'),
(74, 5, 'reciever2', 'Angelito S. Reyes'),
(75, 5, 'reciever_address', '# 53 G. Bunyi BRGY. Buting'),
(76, 5, 'reciever_city', 'Pasig City'),
(77, 5, 'reciever_zip', '1600'),
(78, 5, 'reciever_country', 'Philippines'),
(79, 5, 'reciever_region', 'Metro Manila'),
(80, 5, 'reciever_phone', '(876) 989-8769'),
(81, 5, 'reciever_mobile', '+63 679-856-9559'),
(82, 5, 'reciever_fax', '(868) 968-6869'),
(83, 5, 'reciever_tin', '896-895-689-659'),
(84, 5, 'origin', 'Rizal'),
(85, 5, 'destination', 'Metro Manila'),
(86, 5, 'insurance', 'on'),
(87, 5, 'valuation', 'on'),
(88, 5, 'service', 'Express Legal Letter'),
(89, 5, 'freight', '1500.00'),
(90, 5, 'valuation_amount', '30.00'),
(91, 5, 'insurance_amount', '30.00'),
(92, 5, 'pickup_fee', '0.00'),
(93, 5, 'other', '0.00'),
(94, 5, 'total', '1560.00'),
(95, 5, 'vat_amount', '187.20'),
(96, 5, 'change', '440.00'),
(97, 6, 'reciever', 'Joseph Buarao'),
(98, 6, 'reciever2', 'Mylene Dizon'),
(99, 6, 'reciever_address', '# 89 Purok Kalayaan St. Bagong Silang'),
(100, 6, 'reciever_city', 'Laguna'),
(101, 6, 'reciever_zip', '6712'),
(102, 6, 'reciever_country', 'Philippines'),
(103, 6, 'reciever_region', 'Rizal'),
(104, 6, 'reciever_phone', '(978) 698-5695'),
(105, 6, 'reciever_mobile', '+63 958-595-9558'),
(106, 6, 'reciever_fax', '(589) 589-5858'),
(107, 6, 'reciever_tin', '589-589-589-589'),
(108, 6, 'origin', 'Rizal'),
(109, 6, 'destination', 'Laguna'),
(110, 6, 'insurance', 'on'),
(111, 6, 'valuation', 'on'),
(112, 6, 'service', 'Brown Envelop Letter'),
(113, 6, 'freight', '660.00'),
(114, 6, 'valuation_amount', '13.20'),
(115, 6, 'insurance_amount', '13.20'),
(116, 6, 'pickup_fee', '0.00'),
(117, 6, 'other', '0.00'),
(118, 6, 'total', '686.40'),
(119, 6, 'vat_amount', '82.37'),
(120, 6, 'change', '313.60'),
(121, 7, 'reciever', 'George Daniel Leis'),
(122, 7, 'reciever2', 'Rodolfo J. Leis'),
(123, 7, 'reciever_address', '# 257 D. San Jose St. BRGY. San Isidro'),
(124, 7, 'reciever_city', 'Antipolo City'),
(125, 7, 'reciever_zip', '1780'),
(126, 7, 'reciever_country', 'Philippines'),
(127, 7, 'reciever_region', 'Rizal'),
(128, 7, 'reciever_phone', '(546) 896-8568'),
(129, 7, 'reciever_mobile', '+63 956-985-6975'),
(130, 7, 'reciever_fax', '(755) 689-5895'),
(131, 7, 'reciever_tin', '895-985-895-895'),
(132, 7, 'origin', 'Metro Manila'),
(133, 7, 'destination', 'Rizal'),
(134, 7, 'service', 'General Cargo'),
(135, 7, 'freight', '4800.00'),
(136, 7, 'valuation_amount', '0.00'),
(137, 7, 'insurance_amount', '0.00'),
(138, 7, 'pickup_fee', '0.00'),
(139, 7, 'other', '0.00'),
(140, 7, 'total', '4800.00'),
(141, 7, 'vat_amount', '576.00'),
(142, 7, 'change', '200.00'),
(143, 8, 'reciever', 'RINA SEVILLA'),
(144, 8, 'reciever2', 'wrwqrag'),
(145, 8, 'reciever_address', 'makati'),
(146, 8, 'reciever_city', 'makati'),
(147, 8, 'reciever_zip', '20321'),
(148, 8, 'reciever_country', 'Philippines'),
(149, 8, 'reciever_region', 'ncr'),
(150, 8, 'reciever_phone', '(123) 123-1231'),
(151, 8, 'reciever_mobile', '+63 123-123-1231'),
(152, 8, 'reciever_fax', '(123) 123-123'),
(153, 8, 'reciever_tin', '123-123-123-123'),
(154, 8, 'origin', 'Apayao'),
(155, 8, 'destination', 'Aurora'),
(156, 8, 'insurance', 'on'),
(157, 8, 'service', 'General Cargo'),
(158, 8, 'freight', '29250.00'),
(159, 8, 'valuation_amount', '0.00'),
(160, 8, 'insurance_amount', '585.00'),
(161, 8, 'pickup_fee', '0.00'),
(162, 8, 'other', '0.00'),
(163, 8, 'total', '29835.00'),
(164, 8, 'vat_amount', '3580.20'),
(165, 8, 'change', '2291396.00'),
(166, 9, 'reciever', 'peria'),
(167, 9, 'reciever2', 'claud'),
(168, 9, 'reciever_address', 'dgdgf'),
(169, 9, 'reciever_city', 'fhfghf'),
(170, 9, 'reciever_zip', '4645'),
(171, 9, 'reciever_country', 'Philippines'),
(172, 9, 'reciever_region', '4rtdghd'),
(173, 9, 'reciever_phone', '(467) 4  -'),
(174, 9, 'reciever_mobile', '+63 444-64 -'),
(175, 9, 'reciever_fax', '(445) 46 -'),
(176, 9, 'reciever_tin', '646-456-456-'),
(177, 9, 'origin', 'Batanes'),
(178, 9, 'destination', 'Cavite'),
(179, 9, 'service', 'Express Box Small'),
(180, 9, 'freight', '33000.00'),
(181, 9, 'valuation_amount', '0.00'),
(182, 9, 'insurance_amount', '0.00'),
(183, 9, 'pickup_fee', '0.00'),
(184, 9, 'other', '0.00'),
(185, 9, 'total', '33000.00'),
(186, 9, 'vat_amount', '3960.00'),
(187, 9, 'change', '17000.00'),
(188, 10, 'reciever', 'rtyhnyuhnhu'),
(189, 10, 'reciever2', 'tggbbh '),
(190, 10, 'reciever_address', 'gghhh'),
(191, 10, 'reciever_city', 'yyyy'),
(192, 10, 'reciever_zip', '34333'),
(193, 10, 'reciever_country', 'Philippines'),
(194, 10, 'reciever_region', 'gb hbs xhsbxs'),
(195, 10, 'reciever_phone', '( 54) 546-5466'),
(196, 10, 'reciever_mobile', '+63  46-545-64'),
(197, 10, 'reciever_fax', '( 35) 355-4644'),
(198, 10, 'reciever_tin', '   -647-466-546'),
(199, 10, 'origin', 'Albay'),
(200, 10, 'destination', 'Davao del Norte'),
(201, 10, 'insurance', 'on'),
(202, 10, 'service', 'General Cargo'),
(203, 10, 'freight', '14040.00'),
(204, 10, 'valuation_amount', '0.00'),
(205, 10, 'insurance_amount', '280.80'),
(206, 10, 'pickup_fee', '0.00'),
(207, 10, 'other', '0.00'),
(208, 10, 'total', '14320.80'),
(209, 10, 'vat_amount', '1718.50'),
(210, 10, 'change', '42467.20'),
(211, 11, 'reciever', 'asdhk'),
(212, 11, 'reciever2', 'jhkjhkjh'),
(213, 11, 'reciever_address', 'kjhkjhkj'),
(214, 11, 'reciever_city', 'hkjhkjhkjh'),
(215, 11, 'reciever_zip', '1234'),
(216, 11, 'reciever_country', 'Philippines'),
(217, 11, 'reciever_region', 'jksdnckjzhnx'),
(218, 11, 'reciever_phone', '(987) 987-8979'),
(219, 11, 'reciever_mobile', '+63 897-987-987'),
(220, 11, 'reciever_fax', '(897) 987-897'),
(221, 11, 'reciever_tin', '987-987-897-97'),
(222, 11, 'origin', 'Cagayan'),
(223, 11, 'destination', 'Camarines Norte'),
(224, 11, 'insurance', 'on'),
(225, 11, 'service', 'General Cargo'),
(226, 11, 'freight', '270000.00'),
(227, 11, 'valuation_amount', '0.00'),
(228, 11, 'insurance_amount', '5400.00'),
(229, 11, 'pickup_fee', '0.00'),
(230, 11, 'other', '0.00'),
(231, 11, 'total', '275400.00'),
(232, 11, 'vat_amount', '33048.00'),
(233, 11, 'change', '1.00');

-- --------------------------------------------------------

--
-- Table structure for table `jrs_users`
--

CREATE TABLE IF NOT EXISTS `jrs_users` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) NOT NULL,
  `user_pass` varchar(64) NOT NULL,
  `display_name` varchar(250) NOT NULL,
  `user_status` varchar(50) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `user_login` (`user_login`,`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `jrs_users`
--

INSERT INTO `jrs_users` (`ID`, `user_login`, `user_pass`, `display_name`, `user_status`, `user_id`) VALUES
(1, 'georgeleis2000', '3c1a5d80be00c1581ab850732f5cd39b', 'George Daniel Leis', 'active', 41014),
(2, 'peria', 'dde2e957ce7a28e12d3c86d1b0c5ed0d', 'Claudine Peria', 'active', 0),
(3, 'rina', '3aea9516d222934e35dd30f142fda18c', 'Rinaver Sevilla', 'active', 0),
(4, 'paolo', '969044ea4df948fb0392308cfff9cdce', 'Paulo Redelicia', 'active', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
