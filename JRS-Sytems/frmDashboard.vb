﻿Public Class frmDashboard

    Private Sub btnLogout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogout.Click
        Me.Close()

        is_user_logged_in = False
        logged_in_user = 0

        frmMain.statusLblEmployee.Text = "Logged Out"
        frmMain.LoadLogin()
    End Sub

    Private Sub btnTransact_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTransact.Click
        Me.Close()
        frmTransaction.MdiParent = frmMain
        frmTransaction.Show()
    End Sub

    Private Sub btnNewship_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewship.Click
        Me.Close()
        frmNewShipment.MdiParent = frmMain
        frmNewShipment.Show()
    End Sub

    Private Sub frmDashboard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If is_admin = True Then
            adminBox1.Visible = True
            adminBox1.Enabled = True
            adminBox2.Visible = True
            adminBox2.Enabled = True
            adminBox3.Visible = True
            adminBox3.Enabled = True
            userBox1.Visible = False
            userBox1.Enabled = False
            userBox2.Visible = False
            userBox2.Enabled = False
        Else
            adminBox1.Visible = False
            adminBox1.Enabled = False
            adminBox2.Visible = False
            adminBox2.Enabled = False
            adminBox3.Visible = False
            adminBox3.Enabled = False
            userBox1.Visible = True
            userBox1.Enabled = True
            userBox1.SetBounds(2, 182, 305, 159)
            userBox2.Visible = True
            userBox2.Enabled = True
            userBox2.SetBounds(314, 182, 311, 159)
        End If

        Me.Height = 385
    End Sub
End Class