﻿Imports System.Windows.Forms

Public Class frmMain

    Private Sub ShowNewForm(ByVal sender As Object, ByVal e As EventArgs) Handles NewToolStripMenuItem.Click, NewToolStripButton.Click, NewWindowToolStripMenuItem.Click
        'Create new instance of newShipment form (MdiChild)
        Dim ChildForm As New frmNewShipment()

        'Make it a child of this MDI form before showing it
        ChildForm.MdiParent = Me

        m_ChildFormNumber += 1
        ChildForm.Text = "New Shipment " & m_ChildFormNumber

        ChildForm.Show()

    End Sub

    Private Sub OpenFile(ByVal sender As Object, ByVal e As EventArgs)
        Dim OpenFileDialog As New OpenFileDialog
        OpenFileDialog.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments
        OpenFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*"
        If (OpenFileDialog.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
            Dim FileName As String = OpenFileDialog.FileName
            ' TODO: Add code here to open the file.
        End If
    End Sub

    Private Sub SaveAsToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim SaveFileDialog As New SaveFileDialog
        SaveFileDialog.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments
        SaveFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*"

        If (SaveFileDialog.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
            Dim FileName As String = SaveFileDialog.FileName
            ' TODO: Add code here to save the current contents of the form to a file.
        End If
    End Sub


    Private Sub ExitToolsStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub CutToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        ' Use My.Computer.Clipboard to insert the selected text or images into the clipboard
    End Sub

    Private Sub CopyToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        ' Use My.Computer.Clipboard to insert the selected text or images into the clipboard
    End Sub

    Private Sub PasteToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        'Use My.Computer.Clipboard.GetText() or My.Computer.Clipboard.GetData to retrieve information from the clipboard.
    End Sub

    Private Sub ToolBarToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolBarToolStripMenuItem.Click
        Me.ToolStrip.Visible = Me.ToolBarToolStripMenuItem.Checked
    End Sub

    Private Sub StatusBarToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles StatusBarToolStripMenuItem.Click
        Me.StatusStrip.Visible = Me.StatusBarToolStripMenuItem.Checked
    End Sub

    Private Sub CascadeToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CascadeToolStripMenuItem.Click
        Me.LayoutMdi(MdiLayout.Cascade)
    End Sub

    Private Sub TileVerticalToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles TileVerticalToolStripMenuItem.Click
        Me.LayoutMdi(MdiLayout.TileVertical)
    End Sub

    Private Sub TileHorizontalToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles TileHorizontalToolStripMenuItem.Click
        Me.LayoutMdi(MdiLayout.TileHorizontal)
    End Sub

    Private Sub ArrangeIconsToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ArrangeIconsToolStripMenuItem.Click
        Me.LayoutMdi(MdiLayout.ArrangeIcons)
    End Sub

    Private Sub CloseAllToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CloseAllToolStripMenuItem.Click
        ' Close all child forms of the parent.
        For Each ChildForm As Form In Me.MdiChildren
            ChildForm.Close()
        Next
    End Sub

    Private m_ChildFormNumber As Integer

    Private Sub frmMain_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Dim frmLogin As New login()
        Dim confirm As String = MsgBox("Are you sure you want to exit?", MessageBoxButtons.YesNo, "Confirm")

        If (confirm = DialogResult.Yes) Then
            flashscreen.Close()
            login.Close()
        Else
            If (is_user_logged_in = False) Then
                Dim res As DialogResult = frmLogin.ShowDialog(Me)

                If (res = DialogResult.Cancel) Then
                    Me.Close()
                Else
                    is_user_logged_in = True
                End If
            End If

            e.Cancel = True
        End If

    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ToolStripStatusLabel1.Spring = True
        Me.LoadLogin()
    End Sub

    Protected Sub UpdateChildForms(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs)
        MsgBox("Form closing event fired...")
    End Sub

    Public Sub unloadDashboard(ByRef currentForm As Form)
        If Not Me.ActiveMdiChild Is Nothing Then
            Dim showFrame As Boolean = False

            If Me.MdiChildren.Length <= 1 Then
                If Me.ActiveMdiChild.Name = frmDashboard.Name Then
                    showFrame = False
                End If
                showFrame = True
            Else
                showFrame = False
            End If

            If showFrame = False Then
                frmDashboard.Close()
            Else
                Me.ActiveMdiChild.Focus()
            End If
        End If

        Me.ActiveMdiChild.Focus()
    End Sub

    Public Sub loadDashboard(ByRef currentForm As Form)
        If (Not Me.ActiveMdiChild Is Nothing) Then
            Dim showFrame As Boolean = True
            
            If Me.MdiChildren.Length <= 1 Then
                showFrame = True
            Else
                showFrame = False
            End If

            If showFrame <> False Then
                frmDashboard.MdiParent = Me
                frmDashboard.Show()
            Else
                Me.ActiveMdiChild.Focus()
            End If
        Else
            frmDashboard.MdiParent = Me
            frmDashboard.Show()
        End If

        Me.ActiveMdiChild.Focus()
    End Sub

    Public Sub LoadLogin()
        Dim frmLogin As New login()

        'Close all active MDIChild Forms
        If Me.ActiveMdiChild Is Nothing Then
            'Do nothing
        Else
            Do Until Me.ActiveMdiChild Is Nothing
                Me.ActiveMdiChild.Close()
            Loop
        End If

        Me.ToolStrip.Enabled = False
        Me.MenuStrip.Enabled = False

        frmLogin.txtUsername.Focus()
        Dim res As DialogResult = frmLogin.ShowDialog(Me)

        If (res = DialogResult.Cancel) Then
            Me.Close()
        Else
            is_user_logged_in = True
            Me.loadDashboard(Me)
        End If
    End Sub

    Private Sub OptionsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OptionsToolStripMenuItem.Click
        frmOptions.MdiParent = Me
        frmOptions.Show()
    End Sub

    Private Sub tmrClock_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrClock.Tick
        tslblTime.Text = TimeOfDay
    End Sub

    Private Sub mnuNewUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuNewUser.Click
        For Each f As Form In Application.OpenForms
            If TypeOf f Is frmNewUser Then
                f.Activate()
                Return
            End If
        Next

        Dim frmNewUser As New frmNewUser()

        frmNewUser.MdiParent = Me
        frmNewUser.Show()
    End Sub

    Private Sub mnuNewBranch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuNewBranch.Click
        For Each f As Form In Application.OpenForms
            If TypeOf f Is frmNewBranch Then
                f.Activate()
                Return
            End If
        Next

        Dim frmNewUser As New frmNewBranch()

        frmNewUser.MdiParent = Me
        frmNewUser.Show()
    End Sub

    Private Sub toolBtnTrack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles toolBtnTrack.Click
        frmTransaction.Show()

        frmTransaction.MdiParent = Me
        frmTransaction.txtTransCode.Text = tooltxtTrack.Text
        frmTransaction.SearchFunc()
    End Sub

    Private Sub toolBtnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles toolBtnNew.Click
        'Create new instance of newShipment form (MdiChild)
        Dim ChildForm As New frmNewShipment()

        'Make it a child of this MDI form before showing it
        ChildForm.MdiParent = Me

        m_ChildFormNumber += 1
        ChildForm.Text = "New Shipment " & m_ChildFormNumber

        ChildForm.Show()
    End Sub

    Private Sub LogoutToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LogoutToolStripMenuItem.Click
        is_user_logged_in = False
        logged_in_user = 0

        Me.statusLblEmployee.Text = "Logged Out"
        Me.LoadLogin()
    End Sub

End Class
