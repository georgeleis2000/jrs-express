﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTransaction
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.IDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TrackingCodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AmountDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CashDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PiecesDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.WeightgramsDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ServiceTypeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StatusDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DeliveryScheduleDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustomerNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustomerAddressDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CityDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ZipCodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CountryDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RegionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ContacNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MobileNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FaxNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CashierNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TransactionsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Gdl_jrs_devDataSet = New WindowsApplication1.gdl_jrs_devDataSet()
        Me.txtTransCode = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.TransactionsTableAdapter = New WindowsApplication1.gdl_jrs_devDataSetTableAdapters.TransactionsTableAdapter()
        Me.btnUpdate = New System.Windows.Forms.Button()
        Me.btnSetGPS = New System.Windows.Forms.Button()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TransactionsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Gdl_jrs_devDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDDataGridViewTextBoxColumn, Me.TrackingCodeDataGridViewTextBoxColumn, Me.AmountDataGridViewTextBoxColumn, Me.CashDataGridViewTextBoxColumn, Me.PiecesDataGridViewTextBoxColumn, Me.WeightgramsDataGridViewTextBoxColumn, Me.ServiceTypeDataGridViewTextBoxColumn, Me.StatusDataGridViewTextBoxColumn, Me.DeliveryScheduleDataGridViewTextBoxColumn, Me.CustomerNameDataGridViewTextBoxColumn, Me.CustomerAddressDataGridViewTextBoxColumn, Me.CityDataGridViewTextBoxColumn, Me.ZipCodeDataGridViewTextBoxColumn, Me.CountryDataGridViewTextBoxColumn, Me.RegionDataGridViewTextBoxColumn, Me.ContacNoDataGridViewTextBoxColumn, Me.MobileNoDataGridViewTextBoxColumn, Me.FaxNoDataGridViewTextBoxColumn, Me.CashierNameDataGridViewTextBoxColumn})
        Me.DataGridView1.DataSource = Me.TransactionsBindingSource
        Me.DataGridView1.Location = New System.Drawing.Point(12, 61)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.Size = New System.Drawing.Size(744, 406)
        Me.DataGridView1.TabIndex = 0
        '
        'IDDataGridViewTextBoxColumn
        '
        Me.IDDataGridViewTextBoxColumn.DataPropertyName = "ID"
        Me.IDDataGridViewTextBoxColumn.HeaderText = "ID"
        Me.IDDataGridViewTextBoxColumn.Name = "IDDataGridViewTextBoxColumn"
        Me.IDDataGridViewTextBoxColumn.ReadOnly = True
        '
        'TrackingCodeDataGridViewTextBoxColumn
        '
        Me.TrackingCodeDataGridViewTextBoxColumn.DataPropertyName = "Tracking Code"
        Me.TrackingCodeDataGridViewTextBoxColumn.HeaderText = "Tracking Code"
        Me.TrackingCodeDataGridViewTextBoxColumn.Name = "TrackingCodeDataGridViewTextBoxColumn"
        Me.TrackingCodeDataGridViewTextBoxColumn.ReadOnly = True
        '
        'AmountDataGridViewTextBoxColumn
        '
        Me.AmountDataGridViewTextBoxColumn.DataPropertyName = "Amount"
        Me.AmountDataGridViewTextBoxColumn.HeaderText = "Amount"
        Me.AmountDataGridViewTextBoxColumn.Name = "AmountDataGridViewTextBoxColumn"
        Me.AmountDataGridViewTextBoxColumn.ReadOnly = True
        '
        'CashDataGridViewTextBoxColumn
        '
        Me.CashDataGridViewTextBoxColumn.DataPropertyName = "Cash"
        Me.CashDataGridViewTextBoxColumn.HeaderText = "Cash"
        Me.CashDataGridViewTextBoxColumn.Name = "CashDataGridViewTextBoxColumn"
        Me.CashDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PiecesDataGridViewTextBoxColumn
        '
        Me.PiecesDataGridViewTextBoxColumn.DataPropertyName = "Pieces"
        Me.PiecesDataGridViewTextBoxColumn.HeaderText = "Pieces"
        Me.PiecesDataGridViewTextBoxColumn.Name = "PiecesDataGridViewTextBoxColumn"
        Me.PiecesDataGridViewTextBoxColumn.ReadOnly = True
        '
        'WeightgramsDataGridViewTextBoxColumn
        '
        Me.WeightgramsDataGridViewTextBoxColumn.DataPropertyName = "Weight (grams)"
        Me.WeightgramsDataGridViewTextBoxColumn.HeaderText = "Weight (grams)"
        Me.WeightgramsDataGridViewTextBoxColumn.Name = "WeightgramsDataGridViewTextBoxColumn"
        Me.WeightgramsDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ServiceTypeDataGridViewTextBoxColumn
        '
        Me.ServiceTypeDataGridViewTextBoxColumn.DataPropertyName = "Service Type"
        Me.ServiceTypeDataGridViewTextBoxColumn.HeaderText = "Service Type"
        Me.ServiceTypeDataGridViewTextBoxColumn.Name = "ServiceTypeDataGridViewTextBoxColumn"
        Me.ServiceTypeDataGridViewTextBoxColumn.ReadOnly = True
        '
        'StatusDataGridViewTextBoxColumn
        '
        Me.StatusDataGridViewTextBoxColumn.DataPropertyName = "Status"
        Me.StatusDataGridViewTextBoxColumn.HeaderText = "Status"
        Me.StatusDataGridViewTextBoxColumn.Name = "StatusDataGridViewTextBoxColumn"
        Me.StatusDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DeliveryScheduleDataGridViewTextBoxColumn
        '
        Me.DeliveryScheduleDataGridViewTextBoxColumn.DataPropertyName = "Delivery Schedule"
        Me.DeliveryScheduleDataGridViewTextBoxColumn.HeaderText = "Delivery Schedule"
        Me.DeliveryScheduleDataGridViewTextBoxColumn.Name = "DeliveryScheduleDataGridViewTextBoxColumn"
        Me.DeliveryScheduleDataGridViewTextBoxColumn.ReadOnly = True
        '
        'CustomerNameDataGridViewTextBoxColumn
        '
        Me.CustomerNameDataGridViewTextBoxColumn.DataPropertyName = "Customer Name"
        Me.CustomerNameDataGridViewTextBoxColumn.HeaderText = "Customer Name"
        Me.CustomerNameDataGridViewTextBoxColumn.Name = "CustomerNameDataGridViewTextBoxColumn"
        Me.CustomerNameDataGridViewTextBoxColumn.ReadOnly = True
        '
        'CustomerAddressDataGridViewTextBoxColumn
        '
        Me.CustomerAddressDataGridViewTextBoxColumn.DataPropertyName = "Customer Address"
        Me.CustomerAddressDataGridViewTextBoxColumn.HeaderText = "Customer Address"
        Me.CustomerAddressDataGridViewTextBoxColumn.Name = "CustomerAddressDataGridViewTextBoxColumn"
        Me.CustomerAddressDataGridViewTextBoxColumn.ReadOnly = True
        '
        'CityDataGridViewTextBoxColumn
        '
        Me.CityDataGridViewTextBoxColumn.DataPropertyName = "City"
        Me.CityDataGridViewTextBoxColumn.HeaderText = "City"
        Me.CityDataGridViewTextBoxColumn.Name = "CityDataGridViewTextBoxColumn"
        Me.CityDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ZipCodeDataGridViewTextBoxColumn
        '
        Me.ZipCodeDataGridViewTextBoxColumn.DataPropertyName = "Zip Code"
        Me.ZipCodeDataGridViewTextBoxColumn.HeaderText = "Zip Code"
        Me.ZipCodeDataGridViewTextBoxColumn.Name = "ZipCodeDataGridViewTextBoxColumn"
        Me.ZipCodeDataGridViewTextBoxColumn.ReadOnly = True
        '
        'CountryDataGridViewTextBoxColumn
        '
        Me.CountryDataGridViewTextBoxColumn.DataPropertyName = "Country"
        Me.CountryDataGridViewTextBoxColumn.HeaderText = "Country"
        Me.CountryDataGridViewTextBoxColumn.Name = "CountryDataGridViewTextBoxColumn"
        Me.CountryDataGridViewTextBoxColumn.ReadOnly = True
        '
        'RegionDataGridViewTextBoxColumn
        '
        Me.RegionDataGridViewTextBoxColumn.DataPropertyName = "Region"
        Me.RegionDataGridViewTextBoxColumn.HeaderText = "Region"
        Me.RegionDataGridViewTextBoxColumn.Name = "RegionDataGridViewTextBoxColumn"
        Me.RegionDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ContacNoDataGridViewTextBoxColumn
        '
        Me.ContacNoDataGridViewTextBoxColumn.DataPropertyName = "Contac No_"
        Me.ContacNoDataGridViewTextBoxColumn.HeaderText = "Contac No_"
        Me.ContacNoDataGridViewTextBoxColumn.Name = "ContacNoDataGridViewTextBoxColumn"
        Me.ContacNoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'MobileNoDataGridViewTextBoxColumn
        '
        Me.MobileNoDataGridViewTextBoxColumn.DataPropertyName = "Mobile No_"
        Me.MobileNoDataGridViewTextBoxColumn.HeaderText = "Mobile No_"
        Me.MobileNoDataGridViewTextBoxColumn.Name = "MobileNoDataGridViewTextBoxColumn"
        Me.MobileNoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'FaxNoDataGridViewTextBoxColumn
        '
        Me.FaxNoDataGridViewTextBoxColumn.DataPropertyName = "Fax No_"
        Me.FaxNoDataGridViewTextBoxColumn.HeaderText = "Fax No_"
        Me.FaxNoDataGridViewTextBoxColumn.Name = "FaxNoDataGridViewTextBoxColumn"
        Me.FaxNoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'CashierNameDataGridViewTextBoxColumn
        '
        Me.CashierNameDataGridViewTextBoxColumn.DataPropertyName = "Cashier Name"
        Me.CashierNameDataGridViewTextBoxColumn.HeaderText = "Cashier Name"
        Me.CashierNameDataGridViewTextBoxColumn.Name = "CashierNameDataGridViewTextBoxColumn"
        Me.CashierNameDataGridViewTextBoxColumn.ReadOnly = True
        '
        'TransactionsBindingSource
        '
        Me.TransactionsBindingSource.DataMember = "Transactions"
        Me.TransactionsBindingSource.DataSource = Me.Gdl_jrs_devDataSet
        '
        'Gdl_jrs_devDataSet
        '
        Me.Gdl_jrs_devDataSet.DataSetName = "gdl_jrs_devDataSet"
        Me.Gdl_jrs_devDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'txtTransCode
        '
        Me.txtTransCode.Location = New System.Drawing.Point(496, 23)
        Me.txtTransCode.Name = "txtTransCode"
        Me.txtTransCode.Size = New System.Drawing.Size(179, 20)
        Me.txtTransCode.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(410, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Tracking Code:"
        '
        'btnSearch
        '
        Me.btnSearch.Location = New System.Drawing.Point(681, 21)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(75, 23)
        Me.btnSearch.TabIndex = 3
        Me.btnSearch.Text = "Search"
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'TransactionsTableAdapter
        '
        Me.TransactionsTableAdapter.ClearBeforeFill = True
        '
        'btnUpdate
        '
        Me.btnUpdate.Enabled = False
        Me.btnUpdate.Location = New System.Drawing.Point(12, 21)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(75, 23)
        Me.btnUpdate.TabIndex = 4
        Me.btnUpdate.Text = "UPDATE"
        Me.btnUpdate.UseVisualStyleBackColor = True
        '
        'btnSetGPS
        '
        Me.btnSetGPS.Location = New System.Drawing.Point(93, 21)
        Me.btnSetGPS.Name = "btnSetGPS"
        Me.btnSetGPS.Size = New System.Drawing.Size(122, 23)
        Me.btnSetGPS.TabIndex = 5
        Me.btnSetGPS.Text = "Set GPS Location"
        Me.btnSetGPS.UseVisualStyleBackColor = True
        '
        'frmTransaction
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(769, 479)
        Me.Controls.Add(Me.btnSetGPS)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtTransCode)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "frmTransaction"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "JRS-Transaction"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TransactionsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Gdl_jrs_devDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents txtTransCode As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents Gdl_jrs_devDataSet As WindowsApplication1.gdl_jrs_devDataSet
    Friend WithEvents TransactionsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TransactionsTableAdapter As WindowsApplication1.gdl_jrs_devDataSetTableAdapters.TransactionsTableAdapter
    Friend WithEvents IDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TrackingCodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AmountDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CashDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PiecesDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WeightgramsDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ServiceTypeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents StatusDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DeliveryScheduleDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CustomerNameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CustomerAddressDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CityDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ZipCodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CountryDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RegionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ContacNoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MobileNoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FaxNoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CashierNameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnUpdate As System.Windows.Forms.Button
    Friend WithEvents btnSetGPS As System.Windows.Forms.Button
End Class
