﻿Public Class frmGPSTracker

    Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click
        'Get GeoLocation of the Package
        webWindow.Navigate("wwwapps.ups.com/webtracking/processInputRequest?HTMLVersion=5.0&loc=en_US&Requester=UPSHome&tracknum=" & txtTrackNum.Text)
        txtTrackNum.Visible = False
        btnGo.Visible = False
        btnHide.Text = "Unhide"
    End Sub

    Private Sub txtTrackNum_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTrackNum.TextChanged
        If txtTrackNum.Text.Contains("1z") Then
            btnGo.Enabled = True
        Else
            btnGo.Enabled = False
        End If
    End Sub

    Private Sub btnHide_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHide.Click
        If txtTrackNum.Visible = False Then
            txtTrackNum.Visible = True
            btnGo.Visible = True
            btnHide.Text = "Hide"
        ElseIf txtTrackNum.Visible = True Then
            txtTrackNum.Visible = False
            btnGo.Visible = False
            btnHide.Text = "Unhide"
        End If
    End Sub

End Class